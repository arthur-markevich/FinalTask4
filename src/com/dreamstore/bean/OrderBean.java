package com.dreamstore.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.dreamstore.model.Entity;
import com.dreamstore.model.User;

public class OrderBean extends Entity {

	private static final long serialVersionUID = -3223811704123576802L;

	private Date createdAt;

	private List<OrderItem> orderItems;

	private String status;

	private BigDecimal sum;

	private User user;

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public List<OrderItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "OrderBean [createdAt=" + createdAt + ", orderItems=" + orderItems + ", status=" + status + ", user="
				+ user + "]";
	}

}
