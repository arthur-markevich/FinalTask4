package com.dreamstore.bean;

import java.io.Serializable;

public class OrderItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private ProductBean pb;

	private int quantity;

	
	public OrderItem() {
		super();
	}

	public OrderItem(int quantity) {
		this.quantity = quantity;
	}

	public OrderItem(ProductBean pb, int quantity) {
		super();
		this.pb = pb;
		this.quantity = quantity;
	}

	public ProductBean getPb() {
		return pb;
	}

	public void setPb(ProductBean pb) {
		this.pb = pb;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "OrderItem [pb=" + pb + ", quantity=" + quantity + "]";
	}

}
