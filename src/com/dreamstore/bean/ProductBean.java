package com.dreamstore.bean;

import java.math.BigDecimal;
import java.util.Date;

import com.dreamstore.model.Entity;

public class ProductBean extends Entity {

	private static final long serialVersionUID = 2529305908671482137L;

	private String name;

	private String category;
	
	private long categoryId;

	private BigDecimal price;

	private String description;
	
	private String imageURI;

	private Date createdAt;

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getImageURI() {
		return imageURI;
	}

	public void setImageURI(String imageURI) {
		this.imageURI = imageURI;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ProductBean [name=" + name + ", category=" + category + ", categoryId=" + categoryId + ", price="
				+ price + ", description=" + description + ", imageURI=" + imageURI + ", createdAt=" + createdAt + "]";
	}
	
}
