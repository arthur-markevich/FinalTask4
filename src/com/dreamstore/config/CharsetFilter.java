package com.dreamstore.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class CharsetFilter implements Filter {

	private static final Logger LOG = Logger.getLogger(CharsetFilter.class);

	private String encoding;

	public void destroy() {
		LOG.debug("Filter destruction starts");
		// no op
		LOG.debug("Filter destruction finished");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("Filter starts");

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		LOG.trace("Request uri --> " + httpRequest.getRequestURI());

		String requestEncoding = request.getCharacterEncoding();

		if (null == request.getCharacterEncoding()) {
			LOG.trace("Request encoding = null, set encoding --> " + encoding);
			request.setCharacterEncoding(encoding);
		}
		LOG.debug("Filter finished");
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOG.debug("Filter initialization starts");
		encoding = filterConfig.getInitParameter("requestEncoding");
		LOG.trace("Encoding from web.xml --> " + encoding);
		if (encoding == null) {
			encoding = "UTF-8";
			LOG.trace("Set up encoding --> " + encoding);
		}
		LOG.debug("Filter initialization finished");
	}

}
