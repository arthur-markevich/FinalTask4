package com.dreamstore.config;

public interface Constants {

	public static final int USERNAME_MIN_LENGTH = 4;

	public static final int USERNAME_MAX_LENGTH = 16;

	public static final int USER_PASSWORD_MIN_LENGTH = 8;

	public static final int USER_PASSWORD_MAX_LENGTH = 32;

}
