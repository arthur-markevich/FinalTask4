package com.dreamstore.config;

import java.io.File;


import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

@WebListener
public class FileLocationListener implements ServletContextListener {

	private static final Logger LOG = Logger.getLogger(FileLocationListener.class);
	
	
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		LOG.debug("FileLocationListener destruction starts");
		// no op
		LOG.debug("FileLocationListener destruction finished");
	}



	@Override
	public void contextInitialized(ServletContextEvent sce) {
		LOG.debug("FileLocationListener starts");
		String rootPath = System.getProperty("catalina.home");
		LOG.trace("rootPath --> " + rootPath);

		ServletContext ctx = sce.getServletContext();
		String relativePath = ctx.getInitParameter("upload.location");
		LOG.trace("relativePath --> " + relativePath);
		
		File file = new File(relativePath);
		if (!file.exists()) {
			file.mkdirs();
		}
		LOG.debug("File Directory created to be used for storing files");
		LOG.trace("Directory --> " + file);
		ctx.setAttribute("FILES_DIR_FILE", file);
		ctx.setAttribute("FILES_DIR", relativePath);
		LOG.debug("FileLocationListener initialization finished");
	}

}
