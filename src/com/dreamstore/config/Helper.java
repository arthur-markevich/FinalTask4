package com.dreamstore.config;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper class with common used methods.
 * 
 * @author Markevich Arthur
 *
 */
public final class Helper {

	private static Pattern regexPattern;
	private static Matcher regMatcher;

	/**
	 * Check the string contains data.
	 * 
	 * @param args
	 *            strings to check
	 * 
	 * @return return {@code true} if all strings response {@code string != null &&
	 *         !("".equals(string))}
	 */
	public static boolean nonNull(String... args) {
		for (String s : args) {
			if (s == null || "".equals(s)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check is object not null.
	 * 
	 * @param ob
	 *            object to check
	 * @return true if {@code ob != null} otherwise {@code false}
	 */
	public static boolean nonNull(Object ob) {
		if (ob == null)
			return false;
		return true;
	}

	/**
	 * Validate email string .
	 * 
	 * @param email
	 * @return true if valid
	 */
	public static boolean isValidEmail(String email) {
		regexPattern = Pattern.compile("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$");
		regMatcher = regexPattern.matcher(email);
		return regMatcher.matches() ? true : false;
	}

	/**
	 * Validate phone number string.
	 * 
	 * @param phoneNumber
	 * @return true if valid
	 */
	public static boolean isValidPhoneNumber(String phoneNumber) {
		regexPattern = Pattern.compile("^\\+?[0-9. ()-]{10,13}$");
		regMatcher = regexPattern.matcher(phoneNumber);
		return regMatcher.matches() ? true : false;
	}

	/**
	 * Validate username string.
	 * 
	 * @param username
	 * @returntrue if valid
	 */
	public static boolean isValidUsername(String username) {
		regexPattern = Pattern.compile("^[a-z0-9_-]{4,15}$");
		regMatcher = regexPattern.matcher(username);
		return regMatcher.matches() ? true : false;
	}

	/**
	 * Parse string value to get long value
	 * 
	 * @param number
	 *            number string
	 * @return parsed number or -1 if {@code number} not a number
	 */
	public static long getLong(String number) {
		long l = -1L;
		if (number != null) {
			try {
				l = Long.valueOf(number);
			} catch (NumberFormatException e) {
				l = -1L;
			}
		}
		return l;
	}

	public static BigDecimal getBigDecimal(String number) {
		BigDecimal result = null;
		try {
			result = new BigDecimal(number);
			if (result.compareTo(BigDecimal.ZERO) < 0) {
				return null;
			}
		} catch (NumberFormatException e) {
			//
		}
		return result;
	}

}
