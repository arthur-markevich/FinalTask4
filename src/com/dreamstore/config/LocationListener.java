package com.dreamstore.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.dreamstore.exception.Messages;

public class LocationListener implements ServletContextListener {

	private static final Logger LOG = Logger.getLogger(LocationListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		LOG.debug("LocationListener destruction starts");
		// no op
		LOG.debug("LocationListener destruction finished");
	}

	public void contextInitialized(ServletContextEvent event) {
		LOG.debug("LocationListener starts");
		// obtain file name with locales descriptions
		ServletContext context = event.getServletContext();
		String localesFileName = context.getInitParameter("locales");
		LOG.trace("localesFileName --> " + localesFileName);

		// obtain reale path on server
		String localesFileRealPath = context.getRealPath(localesFileName);
		LOG.trace("localesFileRealPath --> " + localesFileRealPath);

		// load descriptions
		Properties locales = new Properties();
		try {
			locales.load(new FileInputStream(localesFileRealPath));
		} catch (IOException e) {
			LOG.error(Messages.ERR_CANNOT_OBTAIN_LOCALES, e);
			e.printStackTrace();
		}

		// save descriptions to servlet context
		context.setAttribute("locales", locales);
		LOG.trace(locales);
		LOG.debug("LocationListener initialization finished");
	}

}
