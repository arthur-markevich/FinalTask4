package com.dreamstore.config;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class SelfTeg extends SimpleTagSupport {

	private String name;

	public void setName(String name) {
		this.name = name;
	}

	public SelfTeg() {
		super();
	}

	@Override
	public void doTag() throws JspException, IOException {
		String out = "incorrect tag data";
		if (name != null && !("".equals(name))) {
			String s = name.toLowerCase().trim();
			String first = "" + s.charAt(0);
			String res = first.toUpperCase() + s.substring(1);
			out = res;
		}
		getJspContext().getOut().write(out);

	}

}
