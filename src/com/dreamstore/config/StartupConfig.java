package com.dreamstore.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import com.dreamstore.bean.ProductBean;
import com.dreamstore.model.CategoryTranslation;
import com.dreamstore.model.Language;
import com.dreamstore.service.CategoryService;
import com.dreamstore.service.LanguageService;
import com.dreamstore.service.ProductService;

@WebListener
public class StartupConfig implements ServletContextListener {

	private static final Logger LOG = Logger.getLogger(StartupConfig.class);

	private static List<Language> languages;

	private static Map<Long, List<CategoryTranslation>> categoriesMap;

	private static Map<Long, List<ProductBean>> productsMap;

	private static LanguageService languageService = LanguageService.getInstance();

	private static CategoryService categoryService = CategoryService.getInstance();

	private static ProductService productService = ProductService.getInstance();

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		LOG.debug("StartupConfig Listener starts");

		languages = languageService.getAll();
		if (languages != null && languages.size() > 0) {

			categoriesMap = new HashMap<>();
			productsMap = new HashMap<>();

			for (Language language : languages) {
				List<CategoryTranslation> categoryTranslations = categoryService.getAll(language);
				List<ProductBean> productBeans = productService.findAllByLanguage(language);
				categoriesMap.put(language.getId(), categoryTranslations);
				productsMap.put(language.getId(), productBeans);
				LOG.trace("Loaded products size --> " + productsMap.get(language.getId()).size() + " language --> "
						+ language.getName());
				LOG.trace("Loaded categories size --> " + categoriesMap.get(language.getId()).size() + " language --> "
						+ language.getName());
			}
			LOG.debug("StartupConfig Listener finished");

		} else {
			LOG.trace("No languges found: " + languages);
		}

		sce.getServletContext().setAttribute("languages", languages);
		sce.getServletContext().setAttribute("categories", categoriesMap);
		sce.getServletContext().setAttribute("products", productsMap);

	}

}
