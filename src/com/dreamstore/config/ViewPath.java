package com.dreamstore.config;

public interface ViewPath {

	public static final String VIEW = "/WEB-INF/view";

	public static final String HOME = VIEW + "/home_page.jsp";

	public static final String SIGNUP = VIEW + "/signup.jsp";

	public static final String LANGUAGE = VIEW + "/language.jsp";

	public static final String CATEGORIES = VIEW + "/categories.jsp";

	public static final String ADD_CATEGORY = VIEW + "/admin/add_category.jsp";

	public static final String CHANGE_CATEGORY = VIEW + "/admin/change_category.jsp";

	public static final String PRODUCTS = VIEW + "/products.jsp";
	
	public static final String PRODUCTS_BY_CATEGORY = VIEW + "/products_by_category.jsp";

	public static final String PRODUCT_PAGE = VIEW + "/product_page.jsp";

	public static final String CART = VIEW + "/cart.jsp";

	public static final String NO_REGISTERED_PAGE = VIEW + "/no_registered_page.jsp";

	public static final String SUCCESS_ORDER = VIEW + "/user/order_success.jsp";

	public static final String LIST_ORDERS = VIEW + "/admin/list_orders.jsp";

	public static final String ORDER_DETAILS = VIEW + "/admin/order_details.jsp";

	public static final String USER_ORDERS = VIEW + "/admin/user_orders.jsp";

	public static final String USERS_DETAILS = VIEW + "/admin/users_details.jsp";

	public static final String ADD_PRODUCT = VIEW + "/admin/add_product.jsp";

	public static final String CHANGE_PRODUCT = VIEW + "/admin/change_product.jsp";

	public static final String USER_PROFILE = VIEW + "/user/profile.jsp";

	public static final String USERS_ORDERS = VIEW + "/user/orders.jsp";

	public static final String USERS_ORDER_DETAILS = VIEW + "/user/order_details.jsp";

}
