package com.dreamstore.config.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.dreamstore.exception.Messages;
import com.dreamstore.exception.PersistenceException;

public class ConnectionPool {

	private static final Logger LOG = Logger.getLogger(ConnectionPool.class);

	/**
	 * Returns a DB connection from the Pool Connections. Before using this method
	 * you must configure the Date Source and the Connections Pool in your
	 * WEB_APP_ROOT/META-INF/context.xml file.
	 * 
	 * @return DB connection.
	 */
	public static Connection getConnection() throws PersistenceException {
		DataSource ds = null;
		Connection conn = null;
		try {
			Context initContext = new InitialContext();
			ds = (DataSource) initContext.lookup("java:/comp/env/jdbc/webstore");
			conn = ds.getConnection();
			LOG.trace("Data source ==> " + ds);
		} catch (NamingException | SQLException ex) {
			LOG.error(Messages.ERR_CANNOT_OBTAIN_CONNECTION, ex);
			throw new PersistenceException(Messages.ERR_CANNOT_OBTAIN_CONNECTION, ex);
		}
		return conn;
	}

}
