package com.dreamstore.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.dreamstore.model.enumeration.Status;
import com.dreamstore.service.CartService;
import com.dreamstore.service.UserService;
import com.dreamstore.config.Helper;

/**
 * Administrator Controller. Implements methods only for administrator permit.
 */
@WebServlet(urlPatterns = { "/admin/orders", "/admin/users" }, name = "AdminController")
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(AdminController.class);
	
	private static CartService cartService;
	
	private static UserService userService;

	@Override
	public void init() throws ServletException {
		LOG.debug("AdminController init starts");
		cartService = CartService.getInstance();
		userService = UserService.getInstance();
	}

	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("Controller GET method starts");
		
		String requestURI = request.getRequestURI();
		LOG.trace("Request uri --> " + requestURI);

		String action = request.getParameter("action");
		LOG.trace("Request parameter: action  --> " + action);
		
		if (requestURI.endsWith("/orders")) {             // === orders ===
		if (action == null) { 
			cartService.listUsersOrders(request, response);

		} else if (action.equals("view")) { 
			String orderIdStr = request.getParameter("id");
			LOG.trace("Request parameter: id  --> " + orderIdStr);
				Long id = Helper.getLong(orderIdStr);
				cartService.getOrderById(id, request, response);
				
		} else if (action.equals("viewall")) {
				String paramId = request.getParameter("id"); 
				LOG.trace("Request parameter: id  --> " + paramId);
				long id = Helper.getLong(paramId);
				cartService.getOrdersByUserId(id, request, response);
			
		} else if (action.equals("paid")) {
			cartService.changeOrderStatus(Status.PAID, request, response);
		} else if (action.equals("cancel")) {
			cartService.changeOrderStatus(Status.CANCELED, request, response);
		}
		
	} else if (requestURI.endsWith("users")) {		// === users ===
		if (action == null) {
			// go to user list
			userService.getAllUsers(request, response);
		} else if (action.equals("view")) { 
			userService.getOneUser(request, response);
			
		} else if (action.equals("unblock") || action.equals("block")) {
			userService.blockUser(request, response);
		}
	}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
