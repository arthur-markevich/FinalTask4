package com.dreamstore.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dreamstore.config.Helper;
import com.dreamstore.config.ViewPath;
import com.dreamstore.model.User;
import com.dreamstore.service.AuthService;

/**
 * Servlet implementation class AuthController
 */
@WebServlet(name = "auth", urlPatterns = { "/signin", "/signup", "/signout" })
public class AuthController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(AuthController.class);

	private static AuthService authService;

	@Override
	public void init() throws ServletException {
		LOG.debug("AuthController init starts");
		authService = AuthService.getInstance();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String requestURI = request.getRequestURI();

		LOG.debug("Controller GET method starts");
		LOG.trace("request uri --> " + requestURI);

		if (requestURI.endsWith("/signout")) {
			// invalidate session
			Long currentLangId = (Long) request.getSession().getAttribute("lid");
			LOG.trace("Session current language id --> " + currentLangId);

			request.getSession().invalidate();
			LOG.debug("Session language invalidated");

			request.setAttribute("lid", currentLangId);
			LOG.debug("Session language has been setted up --> " + currentLangId);

			LOG.trace("Forward address --> " + ViewPath.HOME);
			response.sendRedirect(request.getContextPath());
			// request.getRequestDispatcher(ViewPath.HOME).forward(request, response);

		} else if (requestURI.endsWith("/signup")) {
			User user = (User) request.getSession().getAttribute("user");
			LOG.trace("Session attribute: user  --> " + user);

			if (user == null) {
				LOG.trace("Forward address --> " + ViewPath.SIGNUP);
				request.getRequestDispatcher(ViewPath.SIGNUP).forward(request, response);
			} else {
				LOG.trace("Redirect address --> " + request.getContextPath() + "/products");
				response.sendRedirect(request.getContextPath() + "/products");
			}

		} else {
			String error = (String) request.getAttribute("errorMsg");
			LOG.trace("Request attribute: action  --> " + error);
			if (error != null) {
				LOG.trace("Forward address --> " + ViewPath.PRODUCTS);
				request.getRequestDispatcher(ViewPath.PRODUCTS).forward(request, response);
			} else {
				LOG.trace("Redirect  --> " + request.getContextPath());
				response.sendRedirect(request.getContextPath());
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String requestURI = request.getRequestURI();
		HttpSession session = request.getSession();

		LOG.debug("Controller POST method starts");
		LOG.trace("request uri --> " + requestURI);

		// signin request
		if (requestURI.endsWith("/signin")) {
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			LOG.trace("request parameter: username  --> " + username);
			LOG.trace("request parameter: password  --> " + password);

			if (Helper.nonNull(username, password)) {
				User user = authService.signin(username, password);
				LOG.trace("Obtained user --> " + user);
				if (user != null) {
					session.setAttribute("user", user);
					LOG.debug("Session attribute 'user' has been setted up");
					doGet(request, response);
				} else {
					LOG.debug("wrong input data, username or password");
					request.setAttribute("errorMsg", "wrong username or password");
					doGet(request, response);
				}
			} else {
				request.setAttribute("errorMsg", "input data is incorrect");
				LOG.debug("input data is incorrectd");
				request.getRequestDispatcher(ViewPath.PRODUCTS).forward(request, response);
				LOG.trace("Forward address --> " + ViewPath.PRODUCTS);
			}
			// signout request
		} else if (requestURI.endsWith("/signup")) {
			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");
			String username = request.getParameter("username");
			String email = request.getParameter("email");
			String phoneNumber = request.getParameter("phonenumber");
			String date = request.getParameter("date");
			String password = request.getParameter("password");
			String passwordRepeat = request.getParameter("passwordRepeat");
			if (Helper.isValidUsername(username) && Helper.isValidEmail(email) && Helper.isValidPhoneNumber(phoneNumber)
					&& password.equals(passwordRepeat)) {

				if (authService.existsByUsername(username)) {
					request.setAttribute("errorMsg", "username already exists");
					LOG.trace("request parameter: username  --> '" + username + "'username already exists");
					LOG.trace("Forward address --> " + ViewPath.SIGNUP);
					request.getRequestDispatcher(ViewPath.SIGNUP).forward(request, response);
				} else {
					User user = authService.signup(firstname, lastname, username, password, email, phoneNumber, date);
					if (user != null) {
						session.setAttribute("user", user);
						doGet(request, response);
					} else {
						request.setAttribute("errorMsg", "error, user not saved");
						LOG.trace("Error,  user not saved, 'user' = " + user);
						LOG.trace("Forward address --> " + ViewPath.SIGNUP);
						request.getRequestDispatcher(ViewPath.SIGNUP).forward(request, response);
					}
				}
			} else {
				request.setAttribute("errorMsg", "input data is incorrect");
				LOG.debug("input data is incorrectd");
				LOG.trace("Forward address --> " + ViewPath.SIGNUP);
				request.getRequestDispatcher(ViewPath.SIGNUP).forward(request, response);
			}
		}

	}

}
