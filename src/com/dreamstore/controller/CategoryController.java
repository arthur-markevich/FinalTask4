package com.dreamstore.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.dreamstore.bean.ProductBean;
import com.dreamstore.config.Helper;
import com.dreamstore.config.ViewPath;
import com.dreamstore.service.CategoryService;
import com.dreamstore.service.ProductService;

/**
 * Servlet implementation class CategoryController
 */
@WebServlet(name = "CategoryController", urlPatterns = { "/admin/categories", "/admin/categories/add",
		"/admin/categories/change", "/categories" })
public class CategoryController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(CategoryController.class);

	private static CategoryService categoryService;

	private static ProductService productService;

	@Override
	public void init() throws ServletException {
		LOG.debug("CategoryController init starts");
		categoryService = CategoryService.getInstance();
		productService = ProductService.getInstance();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("Controller GET method starts");

		String requestURI = request.getRequestURI();
		LOG.trace("Request uri --> " + requestURI);

		if (requestURI.endsWith(request.getContextPath() + "/categories")) {

			long id = Helper.getLong(request.getParameter("id"));
			LOG.trace("Request parameter: id  --> " + request.getParameter("id"));
			if (id > 0) {
				List<ProductBean> products = productService.productsByCategory(id, request);
				request.setAttribute("productsToPage", products);
				request.getRequestDispatcher(ViewPath.PRODUCTS_BY_CATEGORY).forward(request, response);
			} else {
				LOG.debug("wrong category id from request");
				LOG.trace("Forward address --> " + ViewPath.CATEGORIES);
				request.getRequestDispatcher(ViewPath.CATEGORIES).forward(request, response);
			}

		} else {

			String id = request.getParameter("id");
			LOG.trace("Request parameter: id  --> " + id);
			if (id == null) {
				request.getRequestDispatcher(ViewPath.ADD_CATEGORY).forward(request, response);
			} else {
				Long categoryId = Helper.getLong(id);
				if (categoryId.equals(-1L)) {
					request.getRequestDispatcher(ViewPath.ADD_CATEGORY).forward(request, response);
					LOG.trace("Forward address --> " + ViewPath.ADD_CATEGORY);
				} else {
					categoryService.moveToChangeCategory(categoryId, request, response);
				}
			}

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("Controller POST method starts");

		String requestURI = request.getRequestURI();
		LOG.trace("Request uri --> " + requestURI);

		if (requestURI.endsWith("/admin/categories/add")) {

			categoryService.saveCategory(request, response);

		} else if (requestURI.endsWith("/admin/categories/change")) {
			categoryService.updateCategory(request, response);
		} else {
			doGet(request, response);
		}
	}

}
