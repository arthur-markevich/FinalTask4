package com.dreamstore.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.dreamstore.config.Helper;
import com.dreamstore.config.ViewPath;
import com.dreamstore.model.Language;
import com.dreamstore.service.LanguageService;

/**
 * Servlet implementation class LanguageController
 */
@WebServlet(name = "LanguageController", urlPatterns = { "/admin/language", "/admin/language/add", "/lang" })
public class LanguageController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	private static final Logger LOG = Logger.getLogger(LanguageController.class);
	
	
	private static LanguageService languageService;

	@Override
	public void init() throws ServletException {
		LOG.debug("LanguageController init starts");
		languageService = LanguageService.getInstance();
	}
	
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("Controller GET method starts");
		
		String requestURI = request.getRequestURI();
		LOG.trace("Request uri --> " + requestURI);
		
		if (requestURI.endsWith("/lang")) {
			LOG.trace("Request parameter: action  --> " + request.getParameter("id"));
			Long id = Helper.getLong(request.getParameter("id"));
			if (id > 0) {
				request.getSession().setAttribute("languageId", id);
				LOG.trace("Session parameter: languageId  --> " + id);

				Long lid = (Long) request.getSession().getAttribute("lid");
				LOG.trace("Session parameter: lid  --> " + lid);
			}
			
			LOG.trace("Forward address --> " + ViewPath.PRODUCTS);
			request.getRequestDispatcher(ViewPath.PRODUCTS).forward(request, response);

		} else {
			LOG.trace("Forward address --> " + ViewPath.LANGUAGE);
			request.getRequestDispatcher(ViewPath.LANGUAGE).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("Controller POST method starts");
		
		String requestURI = request.getRequestURI();
		LOG.trace("Request uri --> " + requestURI);
		
		if (requestURI.endsWith("/admin/language/add")) {
			String languageName = request.getParameter("name");
			LOG.trace("Request parameter: name  --> " + languageName);
			
			if (Helper.nonNull(languageName)) {
				Language language = languageService.create(languageName);
				LOG.trace("New language has been created --> " + language);
				if (language != null) {
					@SuppressWarnings("unchecked")
					List<Language> languges = (List<Language>) request.getServletContext().getAttribute("languges");
					languges.add(language);
				}
			} // incorrect language input name
			LOG.debug("incorrect language input name");
		}
		doGet(request, response);
	}



}
