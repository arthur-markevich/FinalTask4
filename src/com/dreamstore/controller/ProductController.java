package com.dreamstore.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.dreamstore.bean.ProductBean;
import com.dreamstore.config.Helper;
import com.dreamstore.config.ViewPath;
import com.dreamstore.service.ProductService;

/**
 * Servlet implementation class ProductController
 * 
 */
@WebServlet(urlPatterns = { "/admin/products", "/admin/products/add", "/admin/products/change",
		"/admin/products/delete", "/products" })
@MultipartConfig(maxFileSize = 16177215)
public class ProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ProductController.class);

	private static ProductService productService;

	@Override
	public void init() throws ServletException {
		LOG.debug("ProductController init starts");
		productService = ProductService.getInstance();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("Controller GET method starts");

		String requestURI = request.getRequestURI();
		LOG.trace("Request uri --> " + requestURI);

		String rootPath = getServletConfig().getServletContext().getContextPath();
		LOG.trace("ContextPath  --> " + rootPath);

		if (requestURI.endsWith("/admin/products/add")) {
			LOG.trace("Forward address --> " + ViewPath.ADD_PRODUCT);
			request.getRequestDispatcher(ViewPath.ADD_PRODUCT).forward(request, response);

		} else if (requestURI.endsWith(rootPath + "/products")) {
			LOG.trace("Request parameter: id  --> " + request.getParameter("id"));
			long id = Helper.getLong(request.getParameter("id"));

			String sort = request.getParameter("sort");
			LOG.trace("Request parameter: sort  --> " + sort);
			if (sort != null) {

				productService.sort(sort, request, response);

			} else if (id > 0) {
				productService.getOneProduct(id, request, response);
			} else {

				List<ProductBean> products = productService.getAll(request);

				request.setAttribute("productsToPage", products);
				request.getRequestDispatcher(ViewPath.PRODUCTS).forward(request, response);
			}

		} else if (requestURI.endsWith("/admin/products")) {
			String action = request.getParameter("action");
			LOG.trace("Request parameter: action  --> " + action);

			if (Helper.nonNull(action)) {
				LOG.trace("Request parameter: id  --> " + request.getParameter("id"));
				long id = Helper.getLong(request.getParameter("id"));

				if (action.equals("change")) {
					if (id != -1) {
						productService.moveToChangeProduct(id, request, response);
					}

				}
				if (action.equals("delete")) {
					if (id != -1) {
						productService.deleteProduct(id, request, response);
					}
				}

			} else {
				LOG.trace("Forward address --> " + ViewPath.PRODUCTS);
				request.getRequestDispatcher(ViewPath.PRODUCTS).forward(request, response);

			}

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String requestURI = request.getRequestURI();
		if (requestURI.endsWith("/admin/products/add")) {

			productService.addProduct(request, response);

		} else if (requestURI.endsWith("/admin/products/change")) {

			productService.addProduct(request, response);

		}

	}

}
