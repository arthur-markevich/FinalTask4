package com.dreamstore.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dreamstore.config.ViewPath;
import com.dreamstore.service.CartService;

/**
 * Servlet implementation class ShopingCartController
 */
@WebServlet(urlPatterns = "/cart", name = "cart")
public class ShopingCartController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(ShopingCartController.class);
	
	private static CartService cartService;

	@Override
	public void init() throws ServletException {
		LOG.debug("ShopingCartController init starts");
		cartService = CartService.getInstance();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("Controller GET method starts");
		
		String urlPath = request.getRequestURI();
		LOG.trace("request uri --> " + urlPath);

		String action = request.getParameter("action");
		LOG.trace("Request parameter: action  --> " + action);
		
		HttpSession session = request.getSession();

		if (action != null) {
			if (action.equals("add")) {
				// Service addProduct()
				urlPath = cartService.addProduct(request, response, session);

			} else if (action.equals("remove")) {
				// delete product from cart
				urlPath = cartService.removeProduct(request, response, session);

			} else if (action.equals("buy")) {
				urlPath = cartService.makeOrder(request, response, session);

			} // else = action.equals( other ) ==>
			
		} else {
			// move to cart page
			urlPath = ViewPath.CART;
		}
		LOG.trace("Forward address --> " + urlPath);
		request.getRequestDispatcher(urlPath).forward(request, response);
	}

}
