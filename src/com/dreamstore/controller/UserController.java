package com.dreamstore.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dreamstore.config.ViewPath;
import com.dreamstore.model.User;
import com.dreamstore.service.UserService;

/**
 * Servlet implementation class UserController
 */
@WebServlet(name = "UserController", urlPatterns = { "/user", "/user/profile", "/user/orders" })
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(UserController.class);
	
	
	private static UserService userService;

	@Override
	public void init() throws ServletException {
		LOG.debug("AdminController init starts");
		userService = UserService.getInstance();
	}
	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("Controller GET method starts");
		
		String requestURI = request.getRequestURI();
		LOG.trace("Request uri --> " + requestURI);
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		LOG.trace("Session attribute: user  --> " + user);
		
		if (user != null) {
			if (requestURI.endsWith("/profile")) {
				LOG.trace("Forward address --> " + ViewPath.USER_PROFILE);
				request.getRequestDispatcher(ViewPath.USER_PROFILE).forward(request, response);

			} else if (requestURI.endsWith("/orders")) {
				userService.moveToOrders(user, request, response);

			} else if (requestURI.endsWith("/user")) {
				LOG.trace("Redirect address --> " + request.getContextPath() + "/user/profile");
				response.sendRedirect(request.getContextPath() + "/user/profile");
			
			} 

		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("Controller POST method starts");
		
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		LOG.trace("Session attribute: user  --> " + user);
		
		if (user != null) {
			userService.updateUserData(user, request, response);
		} else {
			LOG.trace("Forward address --> " + ViewPath.PRODUCTS);
			request.getRequestDispatcher(ViewPath.PRODUCTS).forward(request, response);
		}
	}

}
