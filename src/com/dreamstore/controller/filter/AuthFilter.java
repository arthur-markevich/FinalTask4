package com.dreamstore.controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dreamstore.config.ViewPath;
import com.dreamstore.dao.UserDao;
import com.dreamstore.model.User;

public class AuthFilter implements Filter {

	private static final Logger LOG = Logger.getLogger(AuthFilter.class);

	private static UserDao userDao;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOG.debug("FilterConfig filter init starts");
		userDao = UserDao.getInstance();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("Method: doFilter starts");
		final HttpServletRequest req = (HttpServletRequest) request;
		final HttpServletResponse resp = (HttpServletResponse) response;

		HttpSession session = req.getSession(false);

		// Logged user.

		if (nonNull(session) && nonNull(session.getAttribute("user"))) {
			final User user = (User) session.getAttribute("user");

			moveToDashboard(req, resp, user);
		} else if (nonNull(req.getParameter("username")) && nonNull(req.getParameter("password"))) {
			final String username = req.getParameter("username");
			final String password = req.getParameter("password");
			User user = userDao.findByUsernameAndPassword(username, password);
			moveToDashboard(req, resp, user);
		} else {
			moveToDashboard(req, resp, null);
		}

	}

	private void moveToDashboard(HttpServletRequest req, HttpServletResponse resp, User user)
			throws ServletException, IOException {

		if (user != null) {
			if (user.getRoleId() == 1) {
				LOG.trace("roleId:" + user.getRoleId());
				// req.getRequestDispatcher("/WEB-INF/view/admin.jsp").forward(req, resp);

			} else if (user.getRoleId() == 2) {
				LOG.trace("roleId:" + user.getRoleId());

				// req.getRequestDispatcher(ViewPath.PRODUCT).forward(req, resp);
				// go to user page
			}
		}
		req.getRequestDispatcher(ViewPath.HOME).forward(req, resp);
		// resp.sendRedirect(ViewPath.PRODUCT);

	}

	private boolean nonNull(Object ob) {
		if (ob == null)
			return false;
		return true;
	}

}
