package com.dreamstore.controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dreamstore.config.Helper;
import com.dreamstore.model.User;

public class UserFilter implements Filter {
	
	private static final Logger LOG = Logger.getLogger(UserFilter.class);
	
	public void destroy() {
		LOG.debug("Filter destruction starts");
		// no op
		LOG.debug("Filter destruction finished");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("Filter starts");
		
		final HttpServletRequest req = (HttpServletRequest) request;
		final HttpServletResponse resp = (HttpServletResponse) response;

		HttpSession session = req.getSession(false);

		if (Helper.nonNull(session) && Helper.nonNull(session.getAttribute("user"))) {
			User user = (User) session.getAttribute("user");
			if (user != null) {
				LOG.debug("User role user");
				chain.doFilter(request, response);
			} else {
				LOG.debug("User not registered");
				resp.sendRedirect(req.getContextPath());
			}
		} else {
			LOG.debug("Guest");
			resp.sendRedirect(req.getContextPath());
		}

	}

}
