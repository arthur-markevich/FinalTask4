package com.dreamstore.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.dreamstore.config.db.ConnectionPool;
import com.dreamstore.controller.LanguageController;
import com.dreamstore.exception.Messages;
import com.dreamstore.exception.PersistenceException;
import com.dreamstore.model.Category;

public class CategoryDao  {
	
	private static final Logger LOG = Logger.getLogger(CategoryDao.class);
	
	

	private static final String SELECT_ALL = "SELECT * FROM category";

	private static final String INSERT_NEW_CATEGORY = "INSERT INTO category(id) values(DEFAULT)";

	private static CategoryDao instance;

	public static CategoryDao getInstance() {
		if (instance == null) {
			instance = new CategoryDao();
			LOG.debug("CategoryDao instance created");
		}
		return instance;
	}

	private CategoryDao() {
		if (instance != null) {
			throw new IllegalAccessError();
		}
	}

	
	/**
	 * Returns all categories.
	 * 
	 * @return List of categories entities.
	 */
	public List<Category> findAll() throws PersistenceException {
		List<Category> categories = new ArrayList<>();
		try (Connection conn = ConnectionPool.getConnection();
				Statement stm = conn.createStatement()) {
			try (ResultSet rs = stm.executeQuery(SELECT_ALL)) {
				
				while (rs.next()) {
					categories.add(extractFromResultSet(rs));
				}
			} catch (SQLException e) {
				throw new SQLException(e);
			}
			
		} catch (SQLException e) {
			LOG.error(Messages.ERR_CANNOT_OBTAIN_CATEGORIES);
			throw new PersistenceException(e);
		}
		return categories;
	}

	

	/**
	 * Persist category to DB.
	 * 
	 * @return persisted category entity or {@code null}
	 */
	public Category save(Category category) throws PersistenceException {
		if (category == null)
			throw new PersistenceException(new IllegalArgumentException());
		
		try (Connection conn = ConnectionPool.getConnection();
				Statement stm = conn.createStatement()) {

			if (stm.executeUpdate(INSERT_NEW_CATEGORY, Statement.RETURN_GENERATED_KEYS) > 0) {
				try (ResultSet rs = stm.getGeneratedKeys()) {
					if (rs.next()) {
						category.setId(rs.getLong(1));
					}
				} catch (SQLException e) {
					throw new SQLException(e);
				}
				
			}
			conn.commit();
		} catch (SQLException e) {
			category = null;
			LOG.error(Messages.ERR_CANNOT_UPDATE_CATEGORY);
			throw new PersistenceException(e);
		}

		return category;
	}

	/**
	 * Extract category from result set.
	 * 
	 * @param rs result set
	 * @return category entity with data
	 * @throws SQLException
	 */
	private Category extractFromResultSet(ResultSet rs) throws SQLException {
		Category category = new Category(rs.getLong("id"));
		return category;
	}

}
