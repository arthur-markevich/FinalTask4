package com.dreamstore.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dreamstore.config.db.ConnectionPool;
import com.dreamstore.exception.PersistenceException;
import com.dreamstore.model.Category;
import com.dreamstore.model.CategoryTranslation;
import com.dreamstore.model.Language;

public class CategoryTranslationDao implements CrudDao<CategoryTranslation, Category> {

	private static final String GET_ALL_BY_LANGUAGE = "SELECT ct.id, ctt.category_name "
			+ "FROM category_translation ctt, category ct, language l "
			+ "WHERE ctt.category_non_trans_id = ct.id AND ctt.language_id = l.id  AND l.id = ?";

	private static final String CREATE_NEW_CATEGORY_LANGUAGE = "INSERT INTO category_translation(category_non_trans_id, language_id, category_name)"
			+ "VALUES(?, ?, ?)";

	private static final String CREATE_NEW_CATEGORY = "INSERT INTO category(id) values(?)";

	private static final String FIND_TRANSLATIONS_BY_CATEGORY_ID = "SELECT category_name, language_id FROM category_translation WHERE category_non_trans_id = ? ";

	private static final String UPDATE_CATEGORY = "UPDATE category_translation set category_name = ? WHERE language_id = ? AND category_non_trans_id = ? ";

	private static CategoryTranslationDao instance;

	public static CategoryTranslationDao getInstance() {
		if (instance == null) {
			instance = new CategoryTranslationDao();
		}
		return instance;
	}

	private CategoryTranslationDao() {
		if (instance != null) {
			throw new IllegalAccessError();
		}
	}

	/**
	 * Select all categories in special language.
	 * 
	 * @param language
	 *            special language
	 * @return all categories in special language
	 * 
	 * @throws PersistenceException
	 *             if {@code language} == null or in case of SQLException
	 */
	public List<CategoryTranslation> findAll(Language language) throws PersistenceException {
		if (language == null) {
			throw new PersistenceException(new IllegalArgumentException());
		}
		List<CategoryTranslation> categories = new ArrayList<>();
		try (Connection conn = ConnectionPool.getConnection()) {

			PreparedStatement pstm = conn.prepareStatement(GET_ALL_BY_LANGUAGE);
			pstm.setLong(1, language.getId());
			ResultSet rs = pstm.executeQuery();
			while (rs.next()) {
				categories.add(extractFromResultSet(rs, language));
			}

		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		return categories;
	}

	/*
	 * 
	 * Tries with one link for all categories
	 * 
	 */
	private CategoryTranslation extractFromResultSet(ResultSet rs, Language language) throws SQLException {
		CategoryTranslation categoryTranslate = new CategoryTranslation();
		categoryTranslate.setCategoryId(rs.getLong("id"));
		categoryTranslate.setLanguageId(language.getId());
		categoryTranslate.setName(rs.getString("category_name"));
		return categoryTranslate;
	}

	public List<CategoryTranslation> updateBatch(List<CategoryTranslation> categoryTranslations)
			throws PersistenceException {
		if (categoryTranslations == null) {
			throw new PersistenceException(new IllegalArgumentException());
		}
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(UPDATE_CATEGORY)) {
			for (CategoryTranslation t : categoryTranslations) {
				int k = 1;
				pstm.setString(k++, t.getName());
				pstm.setLong(k++, t.getLanguageId());
				pstm.setLong(k++, t.getCategoryId());
				pstm.executeUpdate();
			}
			conn.commit();
		} catch (SQLException e) {
			categoryTranslations = Collections.emptyList();
			throw new PersistenceException(e);
		}

		return categoryTranslations;
	}

	@Override
	public List<CategoryTranslation> findAll() throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CategoryTranslation findById(Category id) throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	public List<CategoryTranslation> saveBatch(List<CategoryTranslation> categoryTranslations) {
		if (categoryTranslations == null) {
			throw new IllegalArgumentException();
		}
		List<CategoryTranslation> savedTranslations = new ArrayList<>();
		try (Connection conn = ConnectionPool.getConnection()) {
			try (PreparedStatement pstmCat = conn.prepareStatement(CREATE_NEW_CATEGORY,
					Statement.RETURN_GENERATED_KEYS);
					PreparedStatement pstmTrans = conn.prepareStatement(CREATE_NEW_CATEGORY_LANGUAGE)) {
				pstmCat.setObject(1, null);

				if (pstmCat.executeUpdate() > 0) {
					ResultSet rsCat = pstmCat.getGeneratedKeys();
					if (rsCat.next()) {
						Category category = new Category(rsCat.getLong(1));
						// saving each translation
						for (CategoryTranslation ct : categoryTranslations) {
							ct.setCategoryId(category.getId());

							int k = 1;
							pstmTrans.setLong(k++, ct.getCategoryId());
							pstmTrans.setLong(k++, ct.getLanguageId());
							pstmTrans.setString(k++, ct.getName());
							if (pstmTrans.executeUpdate() > 0) {
								savedTranslations.add(ct);
							}
						}
						// if inner List not equals out List
						if (categoryTranslations.size() != savedTranslations.size()) {
							// return empty collection in case of error
							savedTranslations = Collections.emptyList();
							conn.rollback();
						}
						// commit transaction
						conn.commit();

					} // if no in rsCat
				} // if you didn't cat parent category PK
			} catch (SQLException e) {
				conn.rollback();
				throw new PersistenceException(e);
			}

		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		return savedTranslations;
	}

	@Override
	public CategoryTranslation save(CategoryTranslation categoryTranslation) throws PersistenceException {
		if (categoryTranslation == null) {
			throw new PersistenceException(new IllegalArgumentException());
		}
		if (categoryTranslation.getLanguageId() == null || categoryTranslation.getCategoryId() == null
				|| categoryTranslation.getName() == null) {
			return null;
		}
		try (Connection conn = ConnectionPool.getConnection()) {
			PreparedStatement pstm = conn.prepareStatement(CREATE_NEW_CATEGORY_LANGUAGE);
			int k = 1;
			pstm.setLong(k++, categoryTranslation.getCategoryId());
			pstm.setLong(k++, categoryTranslation.getLanguageId());
			pstm.setString(k++, categoryTranslation.getName());
			conn.commit();
		} catch (SQLException e) {
			categoryTranslation = null;
			throw new PersistenceException(e);
		}
		return categoryTranslation;
	}

	@Override
	public void delete(CategoryTranslation t) throws PersistenceException {
		// TODO Auto-generated method stub

	}

	public List<CategoryTranslation> findTranslationsByCategoryId(Long categoryId) {
		List<CategoryTranslation> translations = new ArrayList<>();
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(FIND_TRANSLATIONS_BY_CATEGORY_ID)) {
			pstm.setLong(1, categoryId);
			try (ResultSet rs = pstm.executeQuery()) {
				while (rs.next()) {
					CategoryTranslation ct = new CategoryTranslation(categoryId);
					ct.setLanguageId(rs.getLong("language_id"));
					ct.setName(rs.getString("category_name"));
					translations.add(ct);
				}
			} catch (SQLException e) {
				translations = Collections.emptyList();
				throw new SQLException(e);
			}

		} catch (SQLException e) {
			throw new PersistenceException(e);
		}

		return translations;
	}

}
