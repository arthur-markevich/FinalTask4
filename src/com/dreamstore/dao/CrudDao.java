/**
 * 
 */
package com.dreamstore.dao;

import java.util.List;

import com.dreamstore.exception.PersistenceException;

/**
 * Interface gives default crud database operations.
 * 
 * @author Arthur Markevich
 *
 * @param <T>
 *            model type
 * @param <E>
 *            model's id type
 */
public interface CrudDao<T, E> {

	/**
	 * Find all records from the database table.
	 * 
	 * @return List<T> with according objects from database
	 * @throws PersistenceException
	 *             in case of problem of reading database data
	 */
	List<T> findAll() throws PersistenceException;

	/**
	 * Find record from database with special id.
	 * 
	 * @param id
	 *            id of needed record
	 * @return founded field as T object
	 * @throws PersistenceException
	 *             in case of problem of reading database data
	 */
	T findById(E id) throws PersistenceException;

	/**
	 * Insert or update model to database.
	 * 
	 * @param t
	 *            object model to be saved
	 * @return saved object with generated id
	 * @throws PersistenceException
	 *             in case of problem with insertion data to database
	 */
	T save(T t) throws PersistenceException;

	/**
	 * Delete special record from database.
	 * 
	 * @param t
	 *            special model to delete from database record
	 * 
	 * @throws PersistenceException
	 *             in case of problem of manipulation database
	 */
	void delete(T t) throws PersistenceException;

}
