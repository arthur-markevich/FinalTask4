package com.dreamstore.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.dreamstore.config.db.ConnectionPool;
import com.dreamstore.exception.PersistenceException;
import com.dreamstore.model.Language;

public class LanguageDao implements CrudDao<Language, Long> {

	private static final String SELECT_ALL = "SELECT * FROM language";

	private static final String INSERT_NEW_LANGUAGE = "INSERT INTO language(name) VALUES(?)";

	private static final String UPDATE_LANGUAGE = "UPDATE TABLE language SET name = ? WHERE id = ?";

	private static final String FIND_LANGUAGE_BY_ID = SELECT_ALL + "WHERE id = ?";

	private static final String FIND_LANGUAGE_BY_NAME = SELECT_ALL + "WHERE name = ?";

	private static LanguageDao instance;

	public static LanguageDao getInstance() {
		if (instance == null) {
			instance = new LanguageDao();
		}
		return instance;
	}

	private LanguageDao() {
		if (instance != null) {
			throw new IllegalAccessError();
		}
	}

	@Override
	public List<Language> findAll() throws PersistenceException {
		List<Language> languages = new ArrayList<>();
		try (Connection conn = ConnectionPool.getConnection(); Statement stm = conn.createStatement();) {

			ResultSet rs = stm.executeQuery(SELECT_ALL);
			while (rs.next()) {
				languages.add(extractFromResultSet(rs));
			}
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		return languages;
	}

	@Override
	public Language findById(Long id) throws PersistenceException {
		Language language = new Language();
		try (Connection conn = ConnectionPool.getConnection()) {
			PreparedStatement pstm = conn.prepareStatement(FIND_LANGUAGE_BY_ID);
			pstm.setLong(1, id);
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				language = extractFromResultSet(rs);
			}
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		return language;
	}

	@Override
	public Language save(Language language) throws PersistenceException {
		try (Connection conn = ConnectionPool.getConnection()) {
			if (language == null)
				throw new PersistenceException(new IllegalArgumentException());
			ResultSet rs = null;
			PreparedStatement pstm = null;

			int k = 1;
			if (language.getId() == null) {

				// save new language
				pstm = conn.prepareStatement(INSERT_NEW_LANGUAGE, Statement.RETURN_GENERATED_KEYS);
				pstm.setString(k++, language.getName());

				if (pstm.executeUpdate() > 0) {
					rs = pstm.getGeneratedKeys();
					if (rs.next()) {
						language.setId(rs.getLong(1));
					}
				} else {
					language = null;
				}

			} else {

				// update language
				pstm = conn.prepareStatement(UPDATE_LANGUAGE);
				pstm.setString(k++, language.getName());
				pstm.setLong(k++, language.getId());
				if (pstm.executeUpdate() != 1) {
					language = null;
				}
			}
			conn.commit();
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}

		return language;
	}

	public boolean existsByName(String name) {
		try (Connection conn = ConnectionPool.getConnection()) {
			PreparedStatement pstm = conn.prepareStatement(FIND_LANGUAGE_BY_NAME);
			pstm.setString(1, name);
			ResultSet rs = pstm.executeQuery();
			return rs.next();
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public void delete(Language t) throws PersistenceException {
		throw new IllegalAccessError();

	}

	private Language extractFromResultSet(ResultSet rs) throws SQLException {
		Language language = new Language();
		language.setId(rs.getLong("id"));
		language.setName(rs.getString("name"));
		language.setCode(rs.getString("code"));
		return language;
	}

}
