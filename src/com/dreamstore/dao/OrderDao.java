package com.dreamstore.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.dreamstore.bean.OrderBean;
import com.dreamstore.bean.OrderItem;
import com.dreamstore.bean.OrderRowBean;
import com.dreamstore.bean.ProductBean;
import com.dreamstore.config.db.ConnectionPool;
import com.dreamstore.exception.PersistenceException;
import com.dreamstore.model.User;
import com.dreamstore.model.enumeration.Status;

public class OrderDao {

	private static final String FIND_ALL_BY_USER_ID_AND_LANGUAGE = "select o.product_id, pt.product_name, p.price, o.created_at, os.name"
			+ "from order_line o, product p, product_translation pt, language l, user u, order_status os"
			+ "where o.product_id = p.id" + "and o.user_id = u.id" + "and o.status_id = os.id"
			+ "and p.id = pt.product_non_trans_id" + "and pt.language_id = l.id and l.id = ?" + "and u.id = ?";

	private static final String FIND_ALL_BY_LANGUAGE = "select o.product_id, pt.product_name, p.price, o.created_at, os.name"
			+ "from order_line o, product p, product_translation pt, language l, user u, order_status os"
			+ "where o.product_id = p.id" + "and o.user_id = u.id" + "and o.status_id = os.id"
			+ "and p.id = pt.product_non_trans_id" + "and pt.language_id = l.id and l.id = ?";

	private static final String CREATE_ORDER = "insert into orders(created_at, user_id, status_id) values(?, ?, ?)";

	private static final String ADD_ORDER_ITEM = "insert into order_item(order_id, product_id, quantity) values(?, ?, ?);";

	private static final String FIND_ALL_ORDERS = "select o.id, os.name as status, o.created_at, u.id as user_id, u.firstname, u.lastname, u.contact_number"
			+ " from orders o, user u, order_status os" + " where o.user_id = u.id AND o.status_id = os.id ";
	// bean
	private static final String GET_ORDER_BEAN = "select o.id, o.created_at as 'date', os.name as order_status, "
			+ "u.username, u.id as user_id, u.contact_number, u.email, u.firstname, u.lastname, u.blocked "
			+ "from orders o, order_status os, user u " + "where o.status_id = os.id " + "AND o.user_id = u.id ";
	// bean
	private static final String GET_ORDER_ITEM = "select p.id, pt.product_name, p.price, oi.quantity "
			+ "from product p, product_translation pt, order_item oi, orders o, user u, language l "
			+ "where oi.order_id = o.id " + "AND o.user_id = u.id " + "AND oi.product_id = p.id "
			+ "AND pt.product_non_trans_id = p.id AND pt.language_id = l.id AND l.id = ? AND o.id = ?";

	private static final String UPDATE_ORDER_STATUS = "UPDATE orders SET status_id = ? WHERE id = ? ";

	private static final String ALL_USER_ORDERS_SUM = "select SUM(oi.quantity * p.price) as 'total_sum' "
			+ "from product p, orders o, order_item oi, user u\n" + "where oi.order_id = o.id and\n"
			+ "p.id = oi.product_id and\n" + "o.user_id = u.id and\n" + "u.id = ?";

	private static OrderDao instance;

	public static OrderDao getInstance() {
		if (instance == null)
			instance = new OrderDao();
		return instance;
	}

	private OrderDao() {
		if (instance != null)
			throw new IllegalAccessError();
	}

	public boolean updateStatus(int statusId, Long orderId) throws PersistenceException {
		boolean result = false;
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(UPDATE_ORDER_STATUS)) {
			int k = 1;
			pstm.setInt(k++, statusId);
			pstm.setLong(k++, orderId);

			result = (pstm.executeUpdate() == 1);
			conn.commit();
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		return result;
	}

	// correct try-resorces
	public OrderBean createOrder(List<OrderItem> cart, User user) throws PersistenceException {
		if (cart == null || cart.size() == 0 || user == null || user.getId() == null) {
			throw new PersistenceException(new IllegalArgumentException("orderItems == null or less than 1"));
		}
		OrderBean orderBean = new OrderBean();
		orderBean.setUser(user);
		try (Connection conn = ConnectionPool.getConnection()) {
			try (PreparedStatement pstm = conn.prepareStatement(CREATE_ORDER, Statement.RETURN_GENERATED_KEYS)) {
				int k = 1;
				Timestamp date = new Timestamp(new Date().getTime());
				pstm.setTimestamp(k++, date);
				pstm.setLong(k++, user.getId());
				pstm.setInt(k++, Status.REGISTERED.getCode()); // Default order status - REGISTRED
				if (pstm.executeUpdate() > 0) {
					Long orderId = -1L;
					try (ResultSet rs = pstm.getGeneratedKeys()) {
						if (rs.next()) {
							orderId = rs.getLong(1);
							// fill orderBean
							orderBean.setId(orderId);
							orderBean.setCreatedAt(date);
							orderBean.setStatus("REGISTRED");
						}
						if (orderId > 0) {
							// not sure that new pstm is needed
							try (PreparedStatement prstm = conn.prepareStatement(ADD_ORDER_ITEM)) {
								// add all items to created order
								for (OrderItem i : cart) {
									k = 1;
									prstm.setLong(k++, orderId);
									prstm.setLong(k++, i.getPb().getId());
									prstm.setInt(k++, i.getQuantity());
									prstm.executeUpdate();
								}
								orderBean.setOrderItems(cart);
							}

						} // productId less than 0

					} catch (SQLException e) {
						throw new SQLException(e);
					}
				} else {
					//
				}

			} catch (SQLException e) {
				throw new SQLException(e);
			}
			conn.commit(); // commit transaction
		} catch (SQLException e) {
			orderBean = null;
			throw new PersistenceException(e);
		}

		return orderBean;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	public BigDecimal getTotalUserOrdersSum(Long id) throws SQLException {
		BigDecimal sum = BigDecimal.ZERO;
		// BigDecimal one = BigDecimal.ZERO;
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(ALL_USER_ORDERS_SUM)) {
			pstm.setLong(1, id);
			try (ResultSet rs = pstm.executeQuery()) {
				if (rs.next()) {
					// one = rs.getBigDecimal("price").multiply(new
					// BigDecimal(rs.getInt("quantity")));
					// sum = sum.add(one);
					sum = rs.getBigDecimal("total_sum");
				}
			} catch (SQLException e) {
				throw new SQLException(e);
				// TODO: handle exception
			}
		} catch (SQLException e) {
			throw new SQLException(e);
			// TODO: handle exception
		}
		return sum;
	}

	public BigDecimal getOrderSum(Long userId, Long orderId) throws PersistenceException {
		BigDecimal sum = BigDecimal.ZERO;
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(ALL_USER_ORDERS_SUM + " AND o.id = ?")) {
			int k = 1;
			pstm.setLong(k++, userId);
			pstm.setLong(k++, orderId);

			try (ResultSet rs = pstm.executeQuery()) {
				if (rs.next()) {
					sum = rs.getBigDecimal("total_sum");
					System.out.println("rs.next sum: " + sum);
				}
			}

		} catch (SQLException e) {
			throw new PersistenceException(e);
		}

		return sum;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////

	// get one orderBan
	public OrderBean findOneByOrderIdAndLangId(Long orderId, Long langId) {
		if (orderId < 1 || langId < 1) {
			throw new PersistenceException(new IllegalArgumentException());
		}
		OrderBean orderBean = null;
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(GET_ORDER_BEAN + "AND o.id = ?")) {
			int k = 1;
			pstm.setLong(k, orderId);
			try (ResultSet rs = pstm.executeQuery()) {
				if (rs.next()) {
					orderBean = extractOrderBeanFromRs(rs);
					if (orderBean != null) {
						List<OrderItem> orderItems = findAllProductsByOrderIdAndLangId(orderId, langId);
						orderBean.setOrderItems(orderItems);
					}

				} else {
					//
				}

			} catch (SQLException e) {
				throw new PersistenceException(e);
			}
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		return orderBean;
	}

	// find orderItems
	private List<OrderItem> findAllProductsByOrderIdAndLangId(Long orderId, Long langId) throws PersistenceException {
		List<OrderItem> orderItems = new ArrayList<>();

		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(GET_ORDER_ITEM)) {
			int k = 1;
			pstm.setLong(k++, langId);
			pstm.setLong(k++, orderId);
			try (ResultSet rs = pstm.executeQuery()) {
				while (rs.next()) {
					OrderItem oi = new OrderItem();
					oi.setQuantity(rs.getInt("quantity"));
					ProductBean pb = new ProductBean();
					pb.setId(rs.getLong("id"));
					pb.setName(rs.getString("product_name"));
					pb.setPrice(rs.getBigDecimal("price"));
					oi.setPb(pb);
					// add to list
					orderItems.add(oi);
				}
			} catch (SQLException e) {
				orderItems = Collections.emptyList();
				throw new PersistenceException(e);
			}
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}

		return orderItems;
	}

	// find all OrderBeans of user
	public List<OrderBean> findAllByUserIdAndLangId(Long userId, Long langId) throws PersistenceException {
		if (userId == null || langId == null) {
			throw new PersistenceException(new IllegalArgumentException());
		}
		List<OrderBean> orderBeans = new ArrayList<>();
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(FIND_ALL_ORDERS + "AND u.id = ?")) {
			int k = 1;
			pstm.setLong(k, userId);
			try (ResultSet rs = pstm.executeQuery()) {
				while (rs.next()) {
					Long orderId = rs.getLong("id");
					OrderBean orderBean = findOneByOrderIdAndLangId(orderId, langId);
					System.out.println("user id: " + userId + "\norder id: " + orderId);
					orderBean.setSum(getOrderSum(userId, orderId));
					orderBeans.add(orderBean);

				}
			} catch (SQLException e) {
				throw new SQLException(e);
			}
		} catch (SQLException e) {
			orderBeans = Collections.emptyList();
			throw new PersistenceException(e);
		}

		return orderBeans;
	}

	// OrderBean extracted from ReasultSet SQL( GET_ORDER_BEAN + "AND o.id = ? )
	private OrderBean extractOrderBeanFromRs(ResultSet rs) throws SQLException {
		OrderBean orderBean = new OrderBean();
		orderBean.setId(rs.getLong("id"));
		orderBean.setCreatedAt(rs.getTimestamp("date"));
		orderBean.setStatus(rs.getString("order_status"));
		// fill user
		User user = new User();
		user.setId(rs.getLong("user_id"));
		user.setPhoneNumber(rs.getString("contact_number"));
		user.setEmail(rs.getString("email"));
		user.setFirstname(rs.getString("firstname"));
		user.setLastname(rs.getString("lastname"));
		user.setBlocked(rs.getBoolean("blocked"));
		user.setWasted(getTotalUserOrdersSum(user.getId()));
		// set orderBean's user
		orderBean.setUser(user);
		return orderBean;
	}

	public List<OrderRowBean> findAll() throws PersistenceException {
		List<OrderRowBean> list = new ArrayList<>();
		try (Connection conn = ConnectionPool.getConnection(); Statement stm = conn.createStatement()) {
			try (ResultSet rs = stm.executeQuery(FIND_ALL_ORDERS)) {
				while (rs.next()) {
					OrderRowBean or = new OrderRowBean();
					or.setId(rs.getLong("id"));
					or.setStatus(rs.getString("status"));
					or.setCreatedAt(rs.getTimestamp("created_at"));
					or.setUserId(rs.getLong("user_id"));
					or.setFirstname(rs.getString("firstname"));
					or.setLastname(rs.getString("lastname"));
					or.setPhoneNumber(rs.getString("contact_number"));
					list.add(or);
				}

			} catch (SQLException e) {
				list = Collections.emptyList();
				throw new PersistenceException(e);
			}

		} catch (SQLException e) {
			throw new PersistenceException(e);
		}

		return list;
	}

}
