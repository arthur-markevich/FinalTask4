package com.dreamstore.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.dreamstore.bean.ProductBean;
import com.dreamstore.config.db.ConnectionPool;
import com.dreamstore.exception.Messages;
import com.dreamstore.exception.PersistenceException;
import com.dreamstore.model.Language;
import com.dreamstore.model.Product;
import com.dreamstore.model.ProductTranslation;

public class ProductTranslationDao {

	private static final Logger LOG = Logger.getLogger(ProductTranslationDao.class);

	private static final String CREATE_NEW_PRODUCT = "INSERT INTO product(price, category_id, created_at, image_URI) VALUES(?, ?, ?, ?)";

	private static final String CREATE_NEW_PRODUCT_TRANSLATION = "INSERT INTO product_translation(product_non_trans_id, language_id, product_name, description) "
			+ "VALUES(?, ?, ?, ?)";

	private static final String SELECT_ALL_BY_LANGUAGE = "SELECT p.id, category_non_trans_id, pt.product_name, pt.description, p.price, ct.category_name, p.created_at, p.image_URI "
			+ "FROM product p, product_translation pt, language l , category_translation ct "
			+ "WHERE p.id = pt.product_non_trans_id AND pt.language_id = l.id AND p.category_id = ct.category_non_trans_id "
			+ "AND ct.language_id = l.id AND l.id = ? ";

	private static final String FIND_PRODUCT_BY_ID = "SELECT * FROM product WHERE id = ? ";

	private static final String FIND_PRODUCT_TRANSLATIONS_BY_PRODUCT_ID = "SELECT * FROM product_translation WHERE product_non_trans_id = ? ";

	private static final String UPDATE_PRODUCT = "UPDATE product set price = ?, category_id = ? WHERE id = ? ";

	private static final String UPDATE_PRODUCT_WITH_IMG = "UPDATE product set price = ?, category_id = ?, image_URI = ? WHERE id = ? ";

	private static final String UPDATE_PRODUCT_TRANSLATION = "UPDATE product_translation set product_name = ?, description = ? WHERE language_id = ? AND product_non_trans_id = ? ";

	private static final String DELETE_PRODUCT = "DELETE FROM product WHERE id = ? ";

	private static ProductTranslationDao instance;

	public static ProductTranslationDao getInstance() {
		if (instance == null) {
			instance = new ProductTranslationDao();
			LOG.debug("ProductTranslationDao instance created");
		}
		return instance;
	}

	private ProductTranslationDao() {
		if (instance != null) {
			throw new IllegalAccessError();
		}
	}

	/**
	 * @param language
	 *            special language
	 * 
	 * @return list with translations of products
	 * 
	 * @throws PersistenceException
	 *             if SQLException will be thrown
	 * @throws IllegalArgumentException
	 *             if {@code language == null} or {@code language.getId() == null}
	 */
	public List<ProductBean> findAllByLanguage(Language language) throws PersistenceException {
		if (language == null || language.getId() == null) {
			throw new IllegalArgumentException();
		}
		List<ProductBean> productBeans = new ArrayList<>();
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(SELECT_ALL_BY_LANGUAGE)) {

			pstm.setLong(1, language.getId());

			try (ResultSet rs = pstm.executeQuery()) {

				while (rs.next()) {
					ProductBean pb = extractProductBean(rs);
					productBeans.add(pb);
				}

			} catch (SQLException e) {
				throw new SQLException(e);
			}

		} catch (SQLException e) {
			LOG.error(Messages.ERR_OBTAIN_LANGUAGES, e);
			productBeans = Collections.emptyList();
			throw new PersistenceException(e);
		}

		return productBeans;
	}

	/**
	 * Select one ProductBean with needed language.
	 * 
	 * @param productId
	 *            needed product id
	 * @param languageId
	 *            language to represent product bean
	 * @return founded product or {@code null} if product by product id or language
	 *         id will or be found or in case of some exception
	 * @throws PersistenceException
	 */
	public ProductBean findOne(Long productId, Long languageId) throws PersistenceException {
		if (productId == null || languageId == null) {
			throw new IllegalArgumentException();
		}
		ProductBean pb = null;
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(SELECT_ALL_BY_LANGUAGE + "AND p.id = ?")) {
			int k = 1;
			pstm.setLong(k++, languageId);
			pstm.setLong(k++, productId);
			try (ResultSet rs = pstm.executeQuery()) {
				if (rs.next()) {
					pb = extractProductBean(rs);
				}
			} catch (SQLException e) {
				throw new SQLException(e);
			}
		} catch (SQLException e) {
			LOG.error(Messages.ERR_CANNOT_OBTAIN_PRODUCTS, e);
			throw new PersistenceException(e);
		}
		return pb;
	}

	public List<ProductTranslation> findTranslationsByProductId(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("product id can't be null");
		}
		List<ProductTranslation> translations = null;
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(FIND_PRODUCT_TRANSLATIONS_BY_PRODUCT_ID)) {
			pstm.setLong(1, id);
			try (ResultSet rs = pstm.executeQuery()) {
				translations = new ArrayList<>();
				while (rs.next()) {
					ProductTranslation translation = new ProductTranslation();
					translation = extractTranslaton(rs);
					translations.add(translation);
				}
			} catch (SQLException e) {
				translations = Collections.emptyList();
				throw new SQLException(e);
			}
		} catch (SQLException e) {
			LOG.error(Messages.ERR_CANNOT_OBTAIN_TRANSLATIONS, e);
			throw new PersistenceException(e);
		}

		return translations;
	}

	private ProductTranslation extractTranslaton(ResultSet rs) throws SQLException {
		ProductTranslation pt = new ProductTranslation();
		pt.setProductId(rs.getLong("product_non_trans_id"));
		pt.setLanguageId(rs.getLong("language_id"));
		pt.setName(rs.getString("product_name"));
		pt.setDescription(rs.getString("description"));
		return pt;
	}

	public Product getProductById(Long id) throws PersistenceException {
		if (id == null) {
			throw new IllegalArgumentException("product id can't be null");
		}
		Product product = null;
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(FIND_PRODUCT_BY_ID)) {
			pstm.setLong(1, id);
			try (ResultSet rs = pstm.executeQuery()) {
				if (rs.next()) {
					product = extractProduct(rs);
				}
			} catch (SQLException e) {
				throw new SQLException(e);
			}
		} catch (SQLException e) {
			LOG.error(Messages.ERR_CANNOT_OBTAIN_A_PRODUCT, e);
			throw new PersistenceException(e);
		}
		return product;
	}

	/**
	 * Extract product from result set.
	 * 
	 * @param rs
	 * @return Product instance accordingly to ResultSet data.
	 * @throws SQLException
	 */
	private Product extractProduct(ResultSet rs) throws SQLException {
		Product p = new Product();
		p.setId(rs.getLong("id"));
		p.setPrice(rs.getBigDecimal("price"));
		p.setCategoryId(rs.getLong("category_id"));
		p.setCreatedAt(rs.getTimestamp("created_at"));
		p.setImageURI(rs.getString("image_URI"));
		return p;
	}

	public List<ProductTranslation> saveBatch(List<ProductTranslation> productTranslations, Product product)
			throws PersistenceException {
		if (product == null || productTranslations == null || productTranslations.size() == 0) {
			throw new IllegalArgumentException();
		}
		List<ProductTranslation> savedProducts = new ArrayList<>();
		if (product.getId() == null) {
			// create new product
			try (Connection conn = ConnectionPool.getConnection()) {
				try (PreparedStatement pstmProd = conn.prepareStatement(CREATE_NEW_PRODUCT,
						Statement.RETURN_GENERATED_KEYS);
						PreparedStatement pstmTrans = conn.prepareStatement(CREATE_NEW_PRODUCT_TRANSLATION)) {
					Timestamp date = new Timestamp(new Date().getTime());
					int k = 1;
					pstmProd.setBigDecimal(k++, product.getPrice());
					pstmProd.setLong(k++, product.getCategoryId());
					pstmProd.setTimestamp(k++, date);
					pstmProd.setString(k++, product.getImageURI());

					if (pstmProd.executeUpdate() > 0) {
						ResultSet rsProd = pstmProd.getGeneratedKeys();
						if (rsProd.next()) {
							product.setId(rsProd.getLong(1));

							// saving each translation
							for (ProductTranslation pt : productTranslations) {
								pt.setProductId(product.getId());
								k = 1;
								pstmTrans.setLong(k++, pt.getProductId());
								pstmTrans.setLong(k++, pt.getLanguageId());
								pstmTrans.setString(k++, pt.getName());
								pstmTrans.setString(k++, pt.getDescription());
								if (pstmTrans.executeUpdate() > 0) {
									savedProducts.add(pt);
								}
							}
							// if inner List not equals out List
							if (productTranslations.size() != savedProducts.size()) {
								LOG.trace("inner List not equals out List\n" + "in:" + productTranslations.size() + "\n"
										+ "out:" + savedProducts.size());
								// return empty collection in case of error
								savedProducts = Collections.emptyList();
								LOG.debug("connection rollback()");
								conn.rollback();
							}
							// commit transaction
							LOG.debug("transaction commit");
							conn.commit();

						} // if rsCat

					} // if you didn't cat parent category PK
				} catch (SQLException e) {
					conn.rollback();
					throw new SQLException(e);
				}

			} catch (SQLException e) {
				LOG.error("Connection rollback " + e);
				throw new PersistenceException(e);
			}

		} else {
			// update existed product

			// if image was not changed, will be default (default.png)
			String sql = product.getImageURI().equals("default.png") ? UPDATE_PRODUCT : UPDATE_PRODUCT_WITH_IMG;
			try (Connection conn = ConnectionPool.getConnection();
					PreparedStatement pstm = conn.prepareStatement(sql);
					PreparedStatement pstmTrans = conn.prepareStatement(UPDATE_PRODUCT_TRANSLATION)) {
				int k = 1;
				pstm.setBigDecimal(k++, product.getPrice());
				pstm.setLong(k++, product.getCategoryId());
				if (sql.equals(UPDATE_PRODUCT_WITH_IMG)) {
					pstm.setString(k++, product.getImageURI());
				}
				pstm.setLong(k++, product.getId());
				if (pstm.executeUpdate() > 0) {

					// saving each translation
					for (ProductTranslation pt : productTranslations) {
						pt.setProductId(product.getId());
						k = 1;
						pstmTrans.setString(k++, pt.getName());
						pstmTrans.setString(k++, pt.getDescription());
						pstmTrans.setLong(k++, pt.getLanguageId());
						pstmTrans.setLong(k++, pt.getProductId());
						if (pstmTrans.executeUpdate() > 0) {
							savedProducts.add(pt);
						}
					}

				}

				conn.commit();
			} catch (SQLException e) {
				LOG.error(Messages.ERR_CANNOT_SAVE_TRANSLATIONS, e);
				savedProducts = Collections.emptyList();
				throw new PersistenceException(e);
			}

		}

		return savedProducts;
	}

	/**
	 * Delete product from DB.
	 * 
	 * @param product
	 * @throws PersistenceException
	 */
	public void delete(Product product) throws PersistenceException {
		if (product == null || product.getId() == null) {
			throw new IllegalArgumentException();
		}
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(DELETE_PRODUCT)) {
			pstm.setLong(1, product.getId());

			if (pstm.executeUpdate() > 0) {
				LOG.trace("Product with id --> " + product.getId() + " has been deleted");
			}
			conn.commit();
		} catch (SQLException e) {
			LOG.error(Messages.ERR_CANNOT_DELETE_PRODUCT, e);
			throw new PersistenceException(e);
		}

	}

	private ProductBean extractProductBean(ResultSet rs) throws SQLException {
		ProductBean pb = new ProductBean();
		pb.setId(rs.getLong("id"));
		pb.setName(rs.getString("product_name"));
		pb.setDescription(rs.getString("description"));
		pb.setPrice(rs.getBigDecimal("price"));
		pb.setCategory(rs.getString("category_name"));
		pb.setCreatedAt(rs.getTimestamp("created_at"));
		pb.setImageURI(rs.getString("image_URI"));
		pb.setCategoryId(rs.getLong("category_non_trans_id"));
		return pb;
	}

}
