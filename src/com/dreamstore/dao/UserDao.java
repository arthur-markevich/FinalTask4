package com.dreamstore.dao;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.dreamstore.config.db.ConnectionPool;
import com.dreamstore.exception.PersistenceException;
import com.dreamstore.model.User;
import com.dreamstore.security.PasswordEncoder;

public class UserDao implements CrudDao<User, Long> {

	private static final String FIND_ALL_FROM_USER = "SELECT * FROM user";

	private static final String INSERT_USER = "INSERT INTO user(username, password, email, contact_number, firstname, lastname, createdAt, salt, role_id, blocked)"
			+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String UPDATE_USER = "UPDATE user SET email = ?, firstname = ?, lastname = ?, contact_number = ?, username = ? WHERE id =?";

	private static final String FIND_BY_USERNAME = "SELECT * FROM user WHERE username = ?";

	private static final String UPDATE_USER_BLOCKED = "UPDATE user SET blocked = ? WHERE id = ?";

	private static UserDao instance;

	private static OrderDao orderDao = OrderDao.getInstance();

	private static final PasswordEncoder passwordEncoder = new PasswordEncoder();

	public static UserDao getInstance() {
		if (instance == null) {
			instance = new UserDao();
		}
		return instance;
	}

	private UserDao() {
		if (instance != null)
			throw new IllegalAccessError();
	}

	@Override
	public List<User> findAll() throws PersistenceException {
		List<User> users = new ArrayList<>();
		try (Connection conn = ConnectionPool.getConnection(); Statement stm = conn.createStatement()) {
			try (ResultSet rs = stm.executeQuery(FIND_ALL_FROM_USER)) {

				while (rs.next()) {
					users.add(extractUserFromResultSet(rs));
				}

			} catch (SQLException e) {
				users = Collections.emptyList();
				throw new PersistenceException(e);
			}

		} catch (SQLException ex) {
			throw new PersistenceException(ex);
		}
		return users;
	}

	@Override
	public User findById(Long id) throws PersistenceException {
		if (id == null) {
			throw new PersistenceException(new IllegalArgumentException());
		}
		User user = null;
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(FIND_ALL_FROM_USER + " WHERE id = ?")) {
			pstm.setLong(1, id);
			try (ResultSet rs = pstm.executeQuery()) {
				if (rs.next()) {
					user = extractUserFromResultSet(rs);
				}
			} catch (SQLException e) {
				throw new PersistenceException(e);
			}
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		return user;
	}

	/**
	 * Check if database contains user by given username and password.
	 * 
	 * @param username
	 *            username of user
	 * @param password
	 *            user's password
	 * @return if database contains given user return user with all data else return
	 *         {@code null}
	 * @throws PersistenceException
	 */
	public User findByUsernameAndPassword(String username, String password) throws PersistenceException {
		User user = findByUsername(username);
		if (user != null) {
			byte[] salt = user.getSalt();
			try {
				String inputedPassEncoded = passwordEncoder.encode(password, salt);
				if (inputedPassEncoded.equals(user.getPassword())) {
				} else {
					user = null;
				}
			} catch (NoSuchAlgorithmException e) {
				user = null;
				throw new PersistenceException(e);
			}

		}
		return user;
	}

	/**
	 * Find user by username.
	 * 
	 * @param username
	 *            username to be found
	 * @return instance of {@code User} or {@code null} if user with accorded
	 *         username doesn't exists
	 * @throws PersistenceException
	 */
	private User findByUsername(String username) throws PersistenceException {
		User user = null;
		try (Connection conn = ConnectionPool.getConnection()) {
			PreparedStatement pstm = conn.prepareStatement(FIND_BY_USERNAME);
			pstm.setString(1, username);
			ResultSet rs = pstm.executeQuery();
			if (rs.next()) {
				user = extractUserFromResultSet(rs);
			}

		} catch (SQLException ex) {
			throw new PersistenceException(ex);
		}
		return user;
	}

	public boolean existsByUsername(String username) throws PersistenceException {
		try (Connection conn = ConnectionPool.getConnection()) {
			PreparedStatement pstm = conn.prepareStatement(FIND_BY_USERNAME);
			pstm.setString(1, username);
			ResultSet rs = pstm.executeQuery();
			return rs.next();
		} catch (SQLException ex) {
			throw new PersistenceException(ex);
		}
	}

	@Override
	public User save(User user) throws PersistenceException {
		if (user == null) {
			throw new PersistenceException(new IllegalArgumentException());
		}
		int k = 1;
		if (user.getId() != null) {
			// update user
			try (Connection conn = ConnectionPool.getConnection();
					PreparedStatement pstm = conn.prepareStatement(UPDATE_USER)) {
				pstm.setString(k++, user.getEmail());
				pstm.setString(k++, user.getFirstname());
				pstm.setString(k++, user.getLastname());
				pstm.setString(k++, user.getPhoneNumber());
				pstm.setString(k++, user.getUsername());
				pstm.setLong(k++, user.getId());
				pstm.executeUpdate();
				conn.commit();
			} catch (SQLException e) {
				user = null;
				throw new PersistenceException(e);
			}
		} else {
			try (Connection conn = ConnectionPool.getConnection();
					PreparedStatement prstm = conn.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
				// save user
				byte[] salt = passwordEncoder.createSalt();
				String encodedPassword = passwordEncoder.encode(user.getPassword(), salt);
				Timestamp date = new Timestamp(new Date().getTime());

				prstm.setString(k++, user.getUsername());
				prstm.setString(k++, encodedPassword);
				prstm.setString(k++, user.getEmail());
				prstm.setString(k++, user.getPhoneNumber());
				prstm.setString(k++, user.getFirstname());
				prstm.setString(k++, user.getLastname());
				prstm.setTimestamp(k++, date);
				prstm.setBytes(k++, salt);
				prstm.setInt(k++, 2); // Default role is customer
				prstm.setBoolean(k++, user.isBlocked());

				if (prstm.executeUpdate() > 0) {
					try (ResultSet rs = prstm.getGeneratedKeys()) {
						if (rs.next()) {
							user.setId(rs.getLong(1));
							user.setCreatedAt(date);
						}
					} catch (SQLException e) {
						user = null;
						throw new PersistenceException(e);
					}

					conn.commit();
				}
			} catch (SQLException e) {
				user = null;
				throw new PersistenceException(e);
			} catch (NoSuchAlgorithmException e) {
				user = null;
				throw new PersistenceException(e);
			} catch (NoSuchProviderException e) {
				user = null;
				throw new PersistenceException(e);
			}
		}

		return user;
	}

	@Override
	public void delete(User user) throws PersistenceException {
		// TODO Auto-generated method stub

	}

	private User extractUserFromResultSet(ResultSet rs) throws SQLException {
		User user = new User();

		user.setId(rs.getLong("id"));
		user.setUsername(rs.getString("username"));
		user.setPassword(rs.getString("password"));
		user.setEmail(rs.getString("email"));
		user.setPhoneNumber(rs.getString("contact_number"));
		user.setFirstname(rs.getString("firstname"));
		user.setLastname(rs.getString("lastname"));
		user.setCreatedAt(rs.getTimestamp("createdAt"));
		user.setSalt(rs.getBytes("salt"));
		user.setRoleId(rs.getInt("role_id"));
		user.setBlocked(rs.getBoolean("blocked"));
		user.setWasted(orderDao.getTotalUserOrdersSum(user.getId()));

		return user;
	}

	public User updateUserBlock(Long userId, boolean blocked) {
		if (userId == null) {
			throw new PersistenceException(new IllegalArgumentException());
		}
		User user = null;
		try (Connection conn = ConnectionPool.getConnection();
				PreparedStatement pstm = conn.prepareStatement(UPDATE_USER_BLOCKED)) {
			int k = 1;
			pstm.setBoolean(k++, blocked);
			pstm.setLong(k++, userId);
			if (pstm.executeUpdate() > 0) {
				conn.commit();
				user = findById(userId);
			}
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		return user;
	}

}
