package com.dreamstore.exception;

public class PersistenceException extends RuntimeException {
	private static final long serialVersionUID = 7993417363178564116L;

	public PersistenceException() {
		super();
	}

	public PersistenceException(String msg) {
		super(msg);
	}

	public PersistenceException(Exception exception) {
		super(exception);
	}

	public PersistenceException(String msg, Throwable couse) {
		super(msg, couse);
	}

}
