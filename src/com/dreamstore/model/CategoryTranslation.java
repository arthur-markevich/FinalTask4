package com.dreamstore.model;

import java.io.Serializable;

public class CategoryTranslation implements Serializable {

	private static final long serialVersionUID = -4789103490227877161L;

	private Long categoryId;

	private Long languageId;

	private String name;
	
	

	public CategoryTranslation(Long categoryId) {
		super();
		this.categoryId = categoryId;
	}

	public CategoryTranslation() {
		// TODO Auto-generated constructor stub
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CategoryTranslation [categoryId=" + categoryId + ", languageId=" + languageId + ", name=" + name + "]";
	}

	
}
