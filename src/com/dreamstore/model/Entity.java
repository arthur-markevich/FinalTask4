package com.dreamstore.model;

import java.io.Serializable;

/**
 * Root of all entities which have identifier field.
 * 
 * @author Arthur Markevich
 *
 */
public abstract class Entity implements Serializable {

	private static final long serialVersionUID = 8418748315707277235L;

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
