package com.dreamstore.model;

public class Language extends Entity {

	private static final long serialVersionUID = 3696573395641724206L;

	private String name;

	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Language() {

	}

	public Language(Long id) {
		super.setId(id);
	}

	public Language(String name) {
		this.name = name;
	}

	public Language(Long id, String name, String code) {
		setId(id);
		this.name = name;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Language [name=" + name + ", code=" + code + "]";
	}
	
	

}
