package com.dreamstore.model;

import java.util.Date;

import com.dreamstore.model.enumeration.Status;

public class Order extends Entity {

	private static final long serialVersionUID = -3133231151366712485L;

	private User user;

	private Product product;

	private Date createdAt;

	private Status status;

	public Order() {
		super();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Order [user=" + user.getUsername() + ", product=" + product + ", createdAt=" + createdAt + ", status=" + status + "]";
	}

	

}
