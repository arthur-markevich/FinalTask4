package com.dreamstore.model;

import java.math.BigDecimal;
import java.util.Date;

public class Product extends Entity {

	private static final long serialVersionUID = 9000500767384433891L;

	private BigDecimal price;

	private Long categoryId;

	private Date createdAt;
	
	private String imageURI;

	public Product(Long id, BigDecimal price, Long categoryId, String imageURI) {
		this.setId(id);
		this.price = price;
		this.categoryId = categoryId;
		this.imageURI = imageURI;
	}

	public Product(BigDecimal price, Long categoryId) {
		this.price = price;
		this.categoryId = categoryId;
	}

	public Product(BigDecimal price, Long categoryId, String imageURI) {
		super();
		this.price = price;
		this.categoryId = categoryId;
		this.imageURI = imageURI;
	}

	public Product() {
		super();
	}

	public String getImageURI() {
		return imageURI;
	}

	public void setImageURI(String imageURI) {
		this.imageURI = imageURI;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "Product [price=" + price + ", categoryId=" + categoryId + ", createdAt=" + createdAt + ", imageURI="
				+ imageURI + "]";
	}
	
}
