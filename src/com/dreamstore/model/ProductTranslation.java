package com.dreamstore.model;

import java.io.Serializable;

public class ProductTranslation implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long productId;

	private Long languageId;

	private String name;

	private String description;

	public ProductTranslation() {
		super();
	}

	public ProductTranslation(Long product, Long language) {
		this.productId = product;
		this.languageId = language;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
