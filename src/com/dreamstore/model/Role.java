package com.dreamstore.model;

/**
 * @author Arthur Markevich
 *
 */
public class Role extends Entity {

	private static final long serialVersionUID = 4460160885289721560L;

	private String name;

	public Role() {

	}

	public Role(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
