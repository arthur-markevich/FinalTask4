/**
 * 
 */
package com.dreamstore.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Arthur Markevich
 *
 */
public class User extends Entity {

	private static final long serialVersionUID = 5165261234357203891L;

	private String username;
	private String email;
	private String password;
	private String phoneNumber;
	private Date createdAt;

	private String firstname;
	private String lastname;

	private int roleId;
	private boolean blocked;
	private byte[] salt;

	private BigDecimal wasted;

	public User() {
		// TODO Auto-generated constructor stub
	}

	public BigDecimal getWasted() {
		return wasted;
	}

	public void setWasted(BigDecimal wasted) {
		this.wasted = wasted;
	}

	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public byte[] getSalt() {
		return salt;
	}

	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", email=" + email + ", phoneNumber=" + phoneNumber + ", createdAt="
				+ createdAt + ", firstname=" + firstname + ", lastname=" + lastname + ", roleId=" + roleId
				+ ", blocked=" + blocked + "]";
	}

}
