package com.dreamstore.model.enumeration;

public enum Role {
	ADMIN(1), USER(2);

private int code;
	
	private Role(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}
