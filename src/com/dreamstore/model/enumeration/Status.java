package com.dreamstore.model.enumeration;

public enum Status {
	REGISTERED(1), PAID(2), CANCELED(3);
	
	private int code;
	
	private Status(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
 }
