package com.dreamstore.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

/**
 * Class for encrypting user password.
 * 
 * @author Arthur Markevich
 *
 */
public final class PasswordEncoder {

	/**
	 * Encode password.
	 * 
	 * @param password
	 *            to be encoded
	 * 
	 * @param salt
	 *            helps to make password more secure
	 * @return encoded password string
	 * 
	 * @throws NoSuchAlgorithmException
	 *             if no Provider supports a MessageDigestSpi implementation for the
	 *             specified algorithm.
	 *
	 */
	public String encode(String password, final byte[] salt) throws NoSuchAlgorithmException {
		String encodedPassword = null;
		try {
			// Create MessageDigesr instance for MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			// Add password bytes to digest
			md.update(salt);
			// Get the hash's bytes
			byte[] bytes = md.digest(password.getBytes());
			// Convert bytes to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			// Get complete hashed password in hex format
			encodedPassword = sb.toString();
		} catch (NoSuchAlgorithmException ex) {
			throw new NoSuchAlgorithmException(ex);
		}
		return encodedPassword;
	}

	/**
	 * Generate random salt to encode new password. Important you must store this
	 * value to check already stored password.
	 * 
	 * @return random generated salt
	 * 
	 * @throws NoSuchProviderException
	 *             if the specified provider is not registered in the security
	 *             provider list.
	 * @throws NoSuchAlgorithmException
	 *             if a SecureRandomSpi implementation for the specified algorithm
	 *             is not available from the specified provider.
	 */
	public byte[] createSalt() throws NoSuchAlgorithmException, NoSuchProviderException {
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
		byte[] salt = new byte[16];
		// Get random salt
		sr.nextBytes(salt);
		return salt;
	}

}
