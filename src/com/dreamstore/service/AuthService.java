/**
 * 
 */
package com.dreamstore.service;

import org.apache.log4j.Logger;

import com.dreamstore.dao.UserDao;
import com.dreamstore.model.User;
import com.dreamstore.model.enumeration.Role;

/**
 * @author Arthur Markevich
 *
 */
public final class AuthService {

	private static final Logger LOG = Logger.getLogger(AuthService.class);
	
	private final static UserDao userDao = UserDao.getInstance();

	private static AuthService authService = null;

	public static AuthService getInstance() {
		if (authService == null) {
			authService = new AuthService();
			LOG.debug("AuthService instance created");
		}
		return authService;
	}

	private AuthService() {
		if (authService != null) {
			throw new IllegalAccessError();
		}
	}

	
	/**
	 * Register new user to the system.
	 * 
	 * @param firstname
	 * @param lastname
	 * @param username
	 * @param password
	 * @param email
	 * @param phoneNumber
	 * @param date
	 * @return new persisted to DB user or {@code null}
	 */
	public User signup(String firstname, String lastname, String username, String password, String email,
			String phoneNumber, String date) {
		LOG.debug("AuthService method: signup starts");
		
		User user = new User();
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setUsername(username);
		user.setPassword(password);
		user.setEmail(email);
		user.setPhoneNumber(phoneNumber);
		user.setRoleId(Role.USER.getCode()); // Default role user = 2
		user.setBlocked(false);
		
		User newUser = userDao.save(user);
		LOG.trace("Persisted user --> " + newUser);
		
		return newUser;
	}
	
	/**
	 * Find user from DB by giving credentials.
	 * 
	 * @param username
	 * @param password
	 * @return founded user or {@code null}
	 */
	public User signin(String username, String password) {
		LOG.debug("AuthService method: signin starts");
		
		User user = userDao.findByUsernameAndPassword(username, password);
		LOG.trace("Obtained user --> " + user);
		
		return user;
	}

	/**
	 * Check if username already exists in DB.
	 * 
	 * @param username
	 * @return {@code true} if user already exists.
	 */
	public boolean existsByUsername(String username) {
		return userDao.existsByUsername(username);
	}


}
