package com.dreamstore.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dreamstore.bean.OrderBean;
import com.dreamstore.bean.OrderItem;
import com.dreamstore.bean.OrderRowBean;
import com.dreamstore.bean.ProductBean;
import com.dreamstore.config.Helper;
import com.dreamstore.config.ViewPath;
import com.dreamstore.dao.OrderDao;
import com.dreamstore.model.User;
import com.dreamstore.model.enumeration.Status;

public class CartService {

	private static final Logger LOG = Logger.getLogger(CartService.class);

	private static OrderDao orderDao = OrderDao.getInstance();

	private static LanguageService languageService = LanguageService.getInstance();

	private static CartService instance;

	public static CartService getInstance() {
		if (instance == null) {
			instance = new CartService();
			LOG.debug("CartService instance created");
		}
		return instance;
	}

	private CartService() {
		if (instance != null) {
			throw new IllegalAccessError();
		}
	}

	/**
	 * Add product to cart.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return string url path
	 */
	public String addProduct(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		LOG.debug("CartService method: addProduct starts");

		Long currLangId = languageService.getCurrentLanguage(request);

		@SuppressWarnings("unchecked")
		Map<Long, List<ProductBean>> productsMap = (Map<Long, List<ProductBean>>) request.getServletContext()
				.getAttribute("products");
		LOG.trace("Application attribute: products (map)  --> " + productsMap);

		List<ProductBean> pts = productsMap.get(currLangId);
		LOG.trace("Obtained product beans by language id size --> " + pts.size());

		if (session.getAttribute("cart") == null) {
			LOG.trace("Session attribute: 'cart' --> " + null);

			final List<OrderItem> cart = new ArrayList<>();

			LOG.trace("Request parameter: id  --> " + request.getParameter("id"));
			ProductBean prod = findFromProductsTr(pts, Helper.getLong(request.getParameter("id")));

			LOG.trace("Found product by id --> " + prod);

			Integer quantity = 1;

			session.setAttribute("cart", cart);
			LOG.trace("Session attribute: 'cart' has been setted up  --> " + cart);
			if (prod != null) {
				cart.add(new OrderItem(prod, 1));
				session.setAttribute("quantity", quantity);
				LOG.trace("Session attribute: 'quantity' has been setted up  --> " + quantity);
			}

		} else {
			@SuppressWarnings("unchecked")
			List<OrderItem> cart = (List<OrderItem>) session.getAttribute("cart");
			LOG.trace("Session attribute: 'cart' --> " + cart);

			LOG.trace("Request parameter: id  --> " + request.getParameter("id"));

			int index = isExisting(Helper.getLong(request.getParameter("id")), cart);

			ProductBean prod = findFromProductsTr(pts, Helper.getLong(request.getParameter("id")));
			LOG.trace("Found product by id --> " + prod);

			if (prod != null) {
				if (index == -1) {
					cart.add(new OrderItem(prod, 1));
				} else {
					int quantity = cart.get(index).getQuantity() + 1;
					cart.get(index).setQuantity(quantity);
				}

				Integer quantity = (Integer) session.getAttribute("quantity");
				LOG.trace("Session attribute: 'quantity' --> " + quantity);

				if (quantity != null) {
					session.setAttribute("quantity", ++quantity);
				}
				LOG.trace("Session attribute: 'quantity' has been setted up  --> " + quantity);

			}
			session.setAttribute("cart", cart);
			LOG.trace("Session attribute: 'cart' has been setted up --> " + cart);
		}

		return ViewPath.CART;
	}

	/**
	 * Looking for product with special id in the list.
	 * 
	 * @param prods
	 * @param id
	 * @return founded product or {@code null}
	 */
	private ProductBean findFromProductsTr(List<ProductBean> prods, long id) {
		if (prods != null && id > 0) {
			for (ProductBean pb : prods) {
				if (pb.getId() == id) {
					return pb;
				}
			}
		}
		return null;
	}

	/**
	 * Persist new order to DB.
	 * 
	 * @param req
	 * @param resp
	 * @param session
	 * @return string url path
	 */
	public String makeOrder(HttpServletRequest req, HttpServletResponse resp, HttpSession session) {
		LOG.debug("CartService method: makeOrder starts");

		@SuppressWarnings("unchecked")
		List<OrderItem> cart = (List<OrderItem>) session.getAttribute("cart");
		LOG.trace("Session attribute: 'cart' --> " + cart);

		User user = (User) session.getAttribute("user");
		LOG.trace("Session attribute: user  --> " + user);

		if (user == null) {
			return ViewPath.NO_REGISTERED_PAGE;
		}

		if (cart != null) {
			if (Helper.nonNull(user) && Helper.nonNull(cart) && cart.size() > 0 && !user.isBlocked()) {
				OrderBean ob = orderDao.createOrder(cart, user);
				LOG.trace("Persisted ordr --> " + ob);
				if (ob != null) {
					session.removeAttribute("cart");
					session.setAttribute("orderBean", ob); // just for try <-- --
				}
				return ViewPath.CART;

			} else {
				LOG.trace("can't make order:\nuser" + user + "\nblocked" + user.isBlocked() + "\ncart" + cart);
			}
			return ViewPath.CART;

		} else {
			// if cart is not created yet
			return ViewPath.CART;
		}
	}

	/**
	 * Check product in the cart.
	 * 
	 * @param id
	 *            product id
	 * @param cart
	 * @return index of product in the cart or {@code -1}
	 */
	private int isExisting(long id, List<OrderItem> cart) {
		for (int i = 0; i < cart.size(); i++) {

			if (cart.get(i).getPb().getId().equals(id)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Remove product from the cart.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return string url path
	 */
	public String removeProduct(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		LOG.debug("CartService method: removeProduct starts");

		@SuppressWarnings("unchecked")
		List<OrderItem> cart = (List<OrderItem>) session.getAttribute("cart");
		LOG.trace("Session attribute: 'cart' --> " + cart);

		if (cart != null) {
			LOG.trace("Request parameter: action  --> " + request.getParameter("id"));
			int index = isExisting(Integer.valueOf(request.getParameter("id")), cart);

			if (index != -1) {
				int itemQuantity = cart.get(index).getQuantity();

				if (itemQuantity == 1) {
					cart.remove(index);
				} else {
					cart.get(index).setQuantity(--itemQuantity);
				}

				Integer quantity = (Integer) session.getAttribute("quantity");
				LOG.trace("Session attribute: 'quantity' --> " + quantity);
				if (quantity != null) {
					session.setAttribute("quantity", --quantity);
					LOG.trace("Session attribute: 'quantity' has been setted up  --> " + quantity);
				}
				session.setAttribute("cart", cart);
				LOG.trace("Session attribute: 'cart' has been setted up --> " + cart);
			} else {
				LOG.debug("Can't remove product from car: wrong index");
			}
		} else {
			LOG.debug("Can't remove product from cart.");
		}
		return ViewPath.CART;
	}

	/**
	 * Find all orders from all users. And forward to the view.
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void listUsersOrders(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("CartService method: listUsersOrders starts");

		List<OrderRowBean> usersOrders = orderDao.findAll();

		request.setAttribute("ordersList", usersOrders);
		LOG.trace("Session attribute has been setted up: 'ordersList' --> " + usersOrders);

		LOG.trace("Forward address --> " + ViewPath.LIST_ORDERS);
		request.getRequestDispatcher(ViewPath.LIST_ORDERS).forward(request, response);

	}

	/**
	 * Change order status. And forward to the view.
	 * 
	 * @param status
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void changeOrderStatus(Status status, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("CartService method: listUsersOrders starts");

		String orderIdStr = request.getParameter("id");
		LOG.trace("Request parameter: id  --> " + orderIdStr);

		Long orderId = Helper.getLong(orderIdStr);

		if (orderId > 0) {
			int statusId = status.getCode();
			boolean changed = orderDao.updateStatus(statusId, orderId);
			if (changed) {
				request.setAttribute("message", "updated");
			} else {
				request.setAttribute("errorMsg", "error during update");
			}
			getOrderById(orderId, request, response);
		} else {
			request.setAttribute("errorMsg", "wrong order id: " + orderIdStr);

			LOG.trace("Forward address --> " + ViewPath.USERS_ORDERS);
			request.getRequestDispatcher(ViewPath.USERS_ORDERS).forward(request, response);
		}
	}

	/**
	 * Get order by id. And forward to the view.
	 * 
	 * @param orderId
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getOrderById(Long orderId, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("CartService method: getOrderById starts");

		Long langId = languageService.getCurrentLanguage(request);

		if (langId > 0) {

			if (orderId > 0) {
				OrderBean orderBean = orderDao.findOneByOrderIdAndLangId(orderId, langId);
				request.setAttribute("orderBean", orderBean);
			} else {
				request.setAttribute("errorMsg", "Error, wrong order id");
			}

		} else {
			request.setAttribute("errorMsg", "No session languages was found");
		}

		LOG.trace("Forward address --> " + ViewPath.ORDER_DETAILS);
		request.getRequestDispatcher(ViewPath.ORDER_DETAILS).forward(request, response);

	}

	/**
	 * Get all orders of a user. And forward to the view.
	 * 
	 * @param userId
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getOrdersByUserId(Long userId, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("CartService method: getOrdersByUserId starts");

		Long langId = languageService.getCurrentLanguage(request);

		if (langId > 0) {

			if (userId > 0) {
				List<OrderBean> orderBeans = orderDao.findAllByUserIdAndLangId(userId, langId);
				request.setAttribute("ordersList", orderBeans);
				LOG.trace("Request attribute: ordersList has been setted up  --> " + orderBeans);

			} else {
				request.setAttribute("errorMsg", "Error, wrong order id");
			}

		} else {
			request.setAttribute("errorMsg", "No session languages was found");
		}
		LOG.trace("Forward address --> " + ViewPath.USER_ORDERS);
		request.getRequestDispatcher(ViewPath.USER_ORDERS).forward(request, response);
	}

}
