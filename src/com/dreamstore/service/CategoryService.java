package com.dreamstore.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dreamstore.config.Helper;
import com.dreamstore.config.ViewPath;
import com.dreamstore.dao.CategoryDao;
import com.dreamstore.dao.CategoryTranslationDao;
import com.dreamstore.model.Category;
import com.dreamstore.model.CategoryTranslation;
import com.dreamstore.model.Language;

public class CategoryService {

	private static CategoryDao categoryDao = CategoryDao.getInstance();

	private static CategoryTranslationDao categoryTranslationDao = CategoryTranslationDao.getInstance();

	private static LanguageService languageService = LanguageService.getInstance();

	private static CategoryService instance;

	public static CategoryService getInstance() {
		if (instance == null) {
			instance = new CategoryService();
		}
		return instance;
	}

	private CategoryService() {
		if (instance != null) {
			throw new IllegalAccessError();
		}
	}

	public void saveCategory(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<Language> languages = languageService.getSystemLanguages(request);
		if (languages != null) {
			List<CategoryTranslation> catTransList = fetchDataFromRequest(languages, request);

			if (catTransList.size() == languages.size()) {
				// if ok send to persist
				if (addBatch(catTransList).size() > 0) {
					Map<Long, List<CategoryTranslation>> freshTranslations = new HashMap<Long, List<CategoryTranslation>>();
					for (Language l : languages) {
						List<CategoryTranslation> trans = getAll(l);
						freshTranslations.put(l.getId(), trans);
					}
					// refresh cache
					request.getServletContext().setAttribute("categories", freshTranslations);
					response.sendRedirect(request.getContextPath() + "/admin/categories");
				} // else send ERROR

			} else { // error not equals ! ! !
				//
			}
			// List<CategoryTranslation> categories = (List<CategoryTranslation>)
			// getServletContext().getAttribute("categories");

		} // if (language.size() == 0)
	}

	private List<CategoryTranslation> fetchDataFromRequest(List<Language> languages, HttpServletRequest request) {
		List<CategoryTranslation> catTransList = new ArrayList<>();
		// save language by language
		for (Language language : languages) {
			String catTransName = request.getParameter(String.valueOf(language.getId()));
			Long categoryId = Helper.getLong(request.getParameter("id"));

			if (Helper.nonNull(catTransName)) {
				CategoryTranslation catTrans = new CategoryTranslation();
				catTrans.setCategoryId(categoryId);
				catTrans.setLanguageId(language.getId());
				catTrans.setName(catTransName);
				catTransList.add(catTrans);
			} else {
				// work with incorrect input data ! ! ! else { then }
			}
		} // for (end)

		return catTransList;
	}

	public List<CategoryTranslation> addBatch(List<CategoryTranslation> categoryTranslations) {
		return categoryTranslationDao.saveBatch(categoryTranslations);
	}

	public CategoryTranslation addTranslation(CategoryTranslation categoryTranslation) {
		return categoryTranslationDao.save(categoryTranslation);
	}

	public Category createNewCategory(Category category) {
		return categoryDao.save(category);
	}

	public List<CategoryTranslation> getAll(Language language) {
		return categoryTranslationDao.findAll(language);
	}

	public void moveToChangeCategory(Long categoryId, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<CategoryTranslation> translations = categoryTranslationDao.findTranslationsByCategoryId(categoryId);
		request.setAttribute("ct", translations);
		request.getRequestDispatcher(ViewPath.CHANGE_CATEGORY).forward(request, response);

	}

	public void updateCategory(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<Language> languages = languageService.getSystemLanguages(request);
		if (languages != null) {

			List<CategoryTranslation> catTransList = fetchDataFromRequest(languages, request);

			if (catTransList.size() == languages.size()) {
				// if ok send to persist
				List<CategoryTranslation> translations = categoryTranslationDao.updateBatch(catTransList);
				if (translations.size() == languages.size()) {
					// ---------------------------
					// save to cash
					// ----------------------------

					Map<Long, List<CategoryTranslation>> freshTranslations = new HashMap<Long, List<CategoryTranslation>>();
					for (Language l : languages) {
						List<CategoryTranslation> trans = getAll(l);
						freshTranslations.put(l.getId(), trans);
					}
					// refresh cache
					request.getServletContext().setAttribute("categories", freshTranslations);

					response.sendRedirect(request.getContextPath() + "/admin/categories");

				}
			} else { // error not equals ! ! !
				//
			}

		} else {
			//
		}

	}
}
