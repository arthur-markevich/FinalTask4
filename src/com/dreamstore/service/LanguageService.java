package com.dreamstore.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dreamstore.dao.LanguageDao;
import com.dreamstore.model.Language;

public final class LanguageService {

	private static final Logger LOG = Logger.getLogger(LanguageService.class);

	private static LanguageDao languageDao = LanguageDao.getInstance();

	private static LanguageService instance;

	public static LanguageService getInstance() {
		if (instance == null) {
			instance = new LanguageService();
			LOG.debug("LanguageService instance created");
		}
		return instance;
	}

	private LanguageService() {
		if (instance != null) {
			throw new IllegalAccessError();
		}
	}

	/**
	 * Find all languages in DB.
	 * 
	 * @return founded languages or empty list.
	 */
	public List<Language> getAll() {
		LOG.debug("CartService method: getAll starts");
		List<Language> languages = languageDao.findAll();

		LOG.trace("Obtained languages from DB --> " + languages);

		return languages;
	}

	/**
	 * Persist new language to DB.
	 * 
	 * @param languageName
	 * @return persisted language or {@code null}
	 */
	public Language create(String languageName) {
		LOG.debug("CartService method: create starts");
		Language language = new Language(languageName);
		Language newLanguage = languageDao.save(language);
		;

		LOG.trace("Persisted language from DB --> " + newLanguage);

		return newLanguage;
	}

	/**
	 * Obtain current session language id.
	 * 
	 * @param request
	 * @return current language id or 1 if no languageId attribute will be found in
	 *         session
	 */
	public Long getCurrentLanguage(HttpServletRequest request) {
		LOG.debug("CartService method: getCurrentLanguage starts");

		HttpSession session = request.getSession();
		Long languageId = (Long) session.getAttribute("lid");
		LOG.trace("Session attribute: lid  --> " + languageId);

		// if no language id found, set up default id 1L
		if (languageId == null) {
			LOG.debug("No language id found");
			languageId = 1L;
		}
		LOG.trace("Return attribute: lid  --> " + languageId);
		return languageId;
	}

	/**
	 * Get languages loaded to application context.
	 * 
	 * @param request
	 * @return list of founded languages or {@code null}
	 */
	@SuppressWarnings("unchecked")
	public List<Language> getSystemLanguages(HttpServletRequest request) {
		return (List<Language>) request.getServletContext().getAttribute("languages");
	}

}
