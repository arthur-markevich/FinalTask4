package com.dreamstore.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.dreamstore.bean.ProductBean;
import com.dreamstore.config.Helper;
import com.dreamstore.config.ViewPath;
import com.dreamstore.dao.ProductTranslationDao;
import com.dreamstore.model.Language;
import com.dreamstore.model.Product;
import com.dreamstore.model.ProductTranslation;

public class ProductService {

	private static final Logger LOG = Logger.getLogger(ProductService.class);

	private static ProductTranslationDao productTranslationDao = ProductTranslationDao.getInstance();

	private static LanguageService languageService = LanguageService.getInstance();

	private static ProductService instance;

	/**
	 * Correct mime types for image uploading.
	 */
	private static String[] mimeTypes = { "image/gif", "image/jpeg", "image/png", "image/pjpeg" };

	public static ProductService getInstance() {
		if (instance == null) {
			instance = new ProductService();
			LOG.debug("ProductService instance created");
		}
		return instance;
	}

	private ProductService() {
		if (instance != null) {
			throw new IllegalAccessError();
		}
	}

	/**
	 * Get all products of special category.
	 * 
	 * @param categoryId
	 * @param request
	 * @return list of appropriated products of empty list.
	 */
	public List<ProductBean> productsByCategory(long categoryId, HttpServletRequest request) {
		LOG.debug("CartService method: productsByCategory starts");

		List<ProductBean> productBeans = new ArrayList<>();

		@SuppressWarnings("unchecked")
		Map<Long, List<ProductBean>> productsMap = (Map<Long, List<ProductBean>>) request.getServletContext()
				.getAttribute("products");

		long langId = languageService.getCurrentLanguage(request);

		if (productsMap == null) {
			return productBeans;
		}
		// obtain products from cash by language
		List<ProductBean> currentProduct = productsMap.get(langId);
		LOG.trace("Founded products by language id --> " + currentProduct.size());

		for (ProductBean pb : currentProduct) {
			if (pb.getCategoryId() == categoryId) {
				productBeans.add(pb);
			}
		}

		LOG.trace("Founded products by category id --> " + productBeans.size());
		return productBeans;
	}

	/**
	 * Persist new product.
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void addProduct(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		LOG.debug("ProductService method: addProduct starts");

		@SuppressWarnings("unchecked")
		List<Language> languages = (List<Language>) request.getServletContext().getAttribute("languages"); // ApplicationScope

		if (languages != null) {

			String priceStr = request.getParameter("price");
			LOG.trace("Request parameter: price  --> " + priceStr);

			String categoryIdStr = request.getParameter("category");
			LOG.trace("Request parameter: category  --> " + categoryIdStr);

			if (Helper.nonNull(priceStr, categoryIdStr)) {

				// Save image to the server folder
				String imageURI = saveImage(request);

				// if no images saved will be default path
				if (imageURI == null)
					imageURI = "default.png";

				// Trying to get product id
				String productIdStr = request.getParameter("id");
				LOG.trace("Request parameter: id  --> " + productIdStr);

				Long productId = Helper.getLong(productIdStr);

				productId = productId == -1L ? null : productId;

				Long categoryId = -1L;
				BigDecimal price;

				categoryId = Helper.getLong(categoryIdStr);
				price = Helper.getBigDecimal(priceStr);

				if (categoryId > 0 && price != null) {

					Product product = new Product(productId, price, categoryId, imageURI);

					List<ProductTranslation> productTranslations = new ArrayList<>();

					for (Language lang : languages) {
						// save international products view
						String name = request.getParameter("name[" + lang.getId() + "]");
						LOG.trace("Request parameter: name[" + lang.getId() + "] --> " + name);

						String description = request.getParameter("description[" + lang.getId() + "]");
						LOG.trace("Request parameter: description[" + lang.getId() + "]  --> " + description);

						if (Helper.nonNull(name, description)) {
							ProductTranslation pt = new ProductTranslation(product.getId(), lang.getId());
							pt.setName(name);
							pt.setDescription(description);
							productTranslations.add(pt);
						} else {
							// Error if wrong description or name
							LOG.debug("wrong description or name\nname:" + name + "\ndescription:" + description);
						}
					} // loop ended here

					if (productTranslations.size() == languages.size()) {
						List<ProductTranslation> pt = addBatch(productTranslations, product);
						LOG.trace("Persisted product translations  --> " + pt);

						Map<Long, List<ProductBean>> productsMap = new HashMap<>();
						for (Language language : languages) {
							List<ProductBean> productBeans = findAllByLanguage(language);
							productsMap.put(language.getId(), productBeans);
						}
						request.getServletContext().setAttribute("products", productsMap);
						LOG.debug("added new product to application context");

					} else {
						// if different in and out data - Error ! ! !
						request.setAttribute("errorMsg", "Error during saving product");
						LOG.debug(
								"Error during saving product, size of application languages and peristed translation is not equals");
					}

				} else {
					request.setAttribute("errorMsg", "Wrong categoty id or price format");
					LOG.debug("Wrong categoty id or price format");
				}

			} else {
				// if price or category is incorrect strings
				request.setAttribute("errorMsg", "Price or category is incorrect strings");
				LOG.debug("Price or category is incorrect strings");
			}

		} else {
			// do something if no languages found
			LOG.debug("No languages in the system found, please add language first.");
			response.getWriter().println("No languages in the system found, please add language first.");
		}
		LOG.trace("Redirect address --> " + request.getContextPath() + "/products");
		response.sendRedirect(request.getContextPath() + "/products");
	}

	/**
	 * Save image to server path.
	 * 
	 * @param request
	 *            request which has FilePart image parameter.
	 * @return path to saved image or {@code null} if image not being saved
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	public String saveImage(HttpServletRequest request) throws IOException, ServletException {
		LOG.debug("saveImage method:  starts");

		String imageURI = null;
		Part filePart = request.getPart("img");
		LOG.trace("Request parameter: img  --> " + filePart);

		if (filePart != null) {
			if (checkMimeType(filePart.getContentType())) {
				LOG.debug("Load file information");
				LOG.trace("name -->" + filePart.getName());
				LOG.trace("size -->" + filePart.getSize());
				LOG.trace("mime type -->" + filePart.getContentType());

				String[] mtp = filePart.getContentType().split("/");
				String ext = "." + mtp[1];
				String timeStamp = new SimpleDateFormat("dd-MM-yyyy_HHmmss").format(new Date());
				File fileP = new File((String) request.getServletContext().getAttribute("FILES_DIR"));
				if (!fileP.exists()) {
					fileP.mkdirs();
				}
				File file = new File(fileP + File.separator + timeStamp + ext);

				try (InputStream inputStream = filePart.getInputStream()) {
					Files.copy(inputStream, file.toPath());
					LOG.debug("Image has been saved to server path.");

					imageURI = "images" + "/" + timeStamp + ext;
					LOG.trace("imageURI --> " + imageURI);

				} catch (FileNotFoundException e) {
					LOG.error("File not found." + e);
				}
				LOG.trace("Absolute Path at server=" + file.getAbsolutePath());
			} else {
				LOG.trace("wrong image mime type: " + filePart.getContentType());
			}
		}
		return imageURI;
	}

	/**
	 * Check for allowed file mime type.
	 * 
	 * @param mime
	 *            mime type string ("image/png", image/jpeg and so on)
	 * 
	 * @return true if allowed
	 */
	private boolean checkMimeType(String mime) {
		if (mime == null) {
			return false;
		}
		for (String s : mimeTypes) {
			if (s.equals(mime))
				return true;
		}
		return false;
	}

	/**
	 * Move to change product page.
	 * 
	 * @param id
	 *            product id
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	public void moveToChangeProduct(long id, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("CartService method: moveToChangeProduct starts");

		Long languageId = languageService.getCurrentLanguage(request);
		ProductBean productBean = productTranslationDao.findOne(id, languageId);
		LOG.trace("Founded product by id --> " + productBean);

		Product product = productTranslationDao.getProductById(id);
		if (product != null) {
			List<ProductTranslation> translations = productTranslationDao.findTranslationsByProductId(id);
			if (translations.size() > 0) {
				request.setAttribute("languageId", languageId);
				request.setAttribute("pb", productBean);
				request.setAttribute("p", product);
				request.setAttribute("translations", translations);

			} else {
				LOG.trace("ProductTranslation by product id -- > " + id + "not found");
				request.setAttribute("errorMsg", "product translations by product id:" + id + " not found");
			}
		} else {
			LOG.trace("Product with id:" + id + " not found");
			request.setAttribute("errorMsg", "product with id:" + id + " not found");
		}
		request.getRequestDispatcher(ViewPath.CHANGE_PRODUCT).forward(request, response);
	}

	public List<ProductBean> findAllByLanguage(Language language) {
		return productTranslationDao.findAllByLanguage(language);
	}

	/**
	 * Persist new product with different languages.
	 * 
	 * @param productTranslations
	 *            translations of product
	 * @param product
	 *            new product
	 * @return if list persisted return list of all persisted
	 *         {@code productTranslations} else return empty list.
	 */
	public List<ProductTranslation> addBatch(List<ProductTranslation> productTranslations, Product product) {
		LOG.debug("CartService method: addBatch starts");
		return productTranslationDao.saveBatch(productTranslations, product);
	}

	public void getOneProduct(long id, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("CartService method: getOneProduct starts");

		ProductBean pb = getOneProduct(id, request);
		LOG.trace("Foundet ProductInformation --> " + pb);
		if (pb != null) {
			request.setAttribute("pb", pb);
			LOG.trace("Request attribute: 'pb' has been setted up --> " + pb);

			LOG.trace("Forward address --> " + ViewPath.PRODUCT_PAGE);
			request.getRequestDispatcher(ViewPath.PRODUCT_PAGE).forward(request, response);
		}

	}

	public ProductBean getOneProduct(long productId, HttpServletRequest request) {
		LOG.debug("CartService method: getOneProduct starts");
		long languageId = languageService.getCurrentLanguage(request);
		return productTranslationDao.findOne(productId, languageId);
	}

	public void deleteProduct(long id, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Product product = productTranslationDao.getProductById(id);
		if (product != null) {
			productTranslationDao.delete(product);

			@SuppressWarnings("unchecked")
			List<Language> languages = (List<Language>) request.getServletContext().getAttribute("languages");

			Map<Long, List<ProductBean>> productsMap = new HashMap<>();
			for (Language language : languages) {
				List<ProductBean> productBeans = findAllByLanguage(language);
				productsMap.put(language.getId(), productBeans);
			}
			request.getServletContext().setAttribute("products", productsMap);
			LOG.debug("Deleted a product from application context");

			response.sendRedirect(request.getContextPath() + "/products");
		} else {
			request.setAttribute("errorMsg", "Can't delete product. Product not found");
			request.getRequestDispatcher(request.getContextPath() + "/products?id=" + id);
		}
	}

	/**
	 * Product sorting. Sort product and forward to the view.
	 * 
	 * @param sort
	 *            sort type
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void sort(String sort, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<ProductBean> productsToPage = getAll(request);

		switch (sort) {
		case "az":
			productsToPage.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
			break;

		case "za":
			productsToPage.sort((o1, o2) -> o2.getName().compareTo(o1.getName()));
			break;

		case "new":
			productsToPage.sort((o1, o2) -> o2.getCreatedAt().compareTo(o1.getCreatedAt()));
			break;

		case "old":
			productsToPage.sort((o1, o2) -> o1.getCreatedAt().compareTo(o2.getCreatedAt()));
			break;
		case "expensive":
			productsToPage.sort((o1, o2) -> o2.getPrice().compareTo(o1.getPrice()));
			break;

		case "cheap":
			productsToPage.sort((o1, o2) -> o1.getPrice().compareTo(o2.getPrice()));
			break;

		default:
			break;
		}
		request.setAttribute("productsToPage", productsToPage);
		request.getRequestDispatcher(ViewPath.PRODUCTS).forward(request, response);

	}

	public List<ProductBean> getAll(HttpServletRequest request) {
		@SuppressWarnings("unchecked")
		Map<Long, List<ProductBean>> map = (Map<Long, List<ProductBean>>) request.getServletContext()
				.getAttribute("products");
		long currLangId = languageService.getCurrentLanguage(request);

		return map.get(currLangId);
	}

}
