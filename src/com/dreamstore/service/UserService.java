package com.dreamstore.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dreamstore.bean.OrderBean;
import com.dreamstore.config.Helper;
import com.dreamstore.config.ViewPath;
import com.dreamstore.dao.OrderDao;
import com.dreamstore.dao.UserDao;
import com.dreamstore.model.User;

public class UserService {

	private static final Logger LOG = Logger.getLogger(UserService.class);

	private static OrderDao orderDao = OrderDao.getInstance();

	private static UserDao userDao = UserDao.getInstance();

	private static LanguageService languageService = LanguageService.getInstance();

	private static AuthService authService = AuthService.getInstance();

	private static UserService instance;

	public static UserService getInstance() {
		if (instance == null) {
			instance = new UserService();
			LOG.debug("UserService instance created");
		}
		return instance;
	}

	private UserService() {
		if (instance != null) {
			throw new IllegalAccessError();
		}
	}

	/**
	 * Forward user to own orders.
	 * 
	 * @param user
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void moveToOrders(User user, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("CartService method: moveToOrders starts");

		Long langId = languageService.getCurrentLanguage(request);

		String action = request.getParameter("action");
		LOG.trace("Request parameter: action  --> " + action);

		if (action == null) {
			getAllOrders(user, langId, request, response);
		} else if (action.equals("view")) {
			getOrderDetails(user, langId, request, response);
		} else {
			LOG.trace("action is undefined --> " + action);
			LOG.trace("Redirect address --> " + request.getContextPath() + "/user/orders");
			response.sendRedirect(request.getContextPath() + "/user/orders");
		}

	}

	/**
	 * Send user to page with own orders list.
	 * 
	 * @param user
	 *            owner of orders
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getAllOrders(User user, Long langId, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("CartService method: getAllOrders starts");

		List<OrderBean> orders = orderDao.findAllByUserIdAndLangId(user.getId(), langId);
		LOG.trace("Obtaned orders size --> " + orders.size());
		request.setAttribute("orders", orders);

		LOG.trace("Forward address --> " + ViewPath.USERS_ORDERS);
		request.getRequestDispatcher(ViewPath.USERS_ORDERS).forward(request, response);
	}

	/**
	 * Obtain order information. And forward user to the view.
	 * 
	 * @param user
	 * @param langId
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getOrderDetails(User user, Long langId, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("CartService method: getOrderDetails starts");

		LOG.trace("Request parameter: id  --> " + request.getParameter("id"));
		long id = Helper.getLong(request.getParameter("id"));
		if (id != -1) {

			OrderBean orderBean = orderDao.findOneByOrderIdAndLangId(id, langId);
			if (orderBean != null && user.getId().equals(orderBean.getUser().getId())) {
				request.setAttribute("order", orderBean);
				LOG.trace("Request attribute 'order' has been setted up --> " + orderBean);

				LOG.trace("Forward address --> " + ViewPath.USERS_ORDER_DETAILS);
				request.getRequestDispatcher(ViewPath.USERS_ORDER_DETAILS).forward(request, response);
			} else {
				request.setAttribute("errorMsg", "Order id is wrong!");
				getAllOrders(user, langId, request, response);

				LOG.debug("wrong order id: " + user.getId() + " from db: " + orderBean.getUser().getId());
			}

		} else {
			LOG.debug("Order id is wrong");
			request.setAttribute("errorMsg", "Order id is wrong!");
			getAllOrders(user, langId, request, response);
		}

	}

	/**
	 * Update user data in DB.
	 * 
	 * @param user
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void updateUserData(User user, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("CartService method: updateUserData starts");

		String firstname = request.getParameter("firstname");
		LOG.trace("Request parameter: firstname  --> " + firstname);

		String lastname = request.getParameter("lastname");
		LOG.trace("Request parameter: lastname  --> " + lastname);

		String phoneNumber = request.getParameter("phonenumber");
		LOG.trace("Request parameter: phoneNumber  --> " + phoneNumber);

		String email = request.getParameter("email");
		LOG.trace("Request parameter: email  --> " + email);

		String username = request.getParameter("username");
		LOG.trace("Request parameter: username  --> " + username);

		String password = request.getParameter("password");

		boolean notEmptyFeilds = Helper.nonNull(firstname, lastname, phoneNumber, email, username, password);

		
		boolean sameUsername = user.getUsername().equalsIgnoreCase(username);
		
		if (!sameUsername) {
			sameUsername = !authService.existsByUsername(username);
		}
		
		if (sameUsername) {

			// if user data input is not empty
			if (notEmptyFeilds) {
				if (Helper.isValidEmail(email) && Helper.isValidPhoneNumber(phoneNumber)
						&& Helper.isValidUsername(username)) {
					// try to find user's credentials
					User dbUser = userDao.findByUsernameAndPassword(user.getUsername(), password);
					if (dbUser != null) {
						dbUser.setEmail(email);
						dbUser.setFirstname(firstname);
						dbUser.setLastname(lastname);
						dbUser.setPhoneNumber(phoneNumber);
						dbUser.setUsername(username);

						User updatedUser = userDao.save(dbUser);
						if (updatedUser != null) {
							// change user in the session with new user data
							HttpSession session = request.getSession();
							session.setAttribute("user", updatedUser);
//							request.setAttribute("message", "-- Saved --");
							response.sendRedirect(request.getContextPath() + "/user/profile");
						}

					} else {
						request.setAttribute("errorMsg", "Password is incorrect!");
						request.getRequestDispatcher(ViewPath.USER_PROFILE).forward(request, response);
						// send back with error message
						LOG.debug("password is incorrect");
					}
				} else {
					// send back with error message
					request.setAttribute("errorMsg", "Input data is incorrect!");
					request.getRequestDispatcher(ViewPath.USER_PROFILE).forward(request, response);
					LOG.debug("input data is incorrect");
				}
			} else {
				// send back with error message
				request.setAttribute("errorMsg", "Input data is incorrect!");
				request.getRequestDispatcher(ViewPath.USER_PROFILE).forward(request, response);
				LOG.debug("input data is incorrect");
			}

		} else {
			// exist by username
			request.setAttribute("errorMsg", "Username already exists!");
			request.getRequestDispatcher(ViewPath.USER_PROFILE).forward(request, response);
			LOG.debug("Username already exists.");
		}

	}

	/**
	 * Find all users from DB.
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getAllUsers(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("AuthService method: getAllUsers starts");
		
		List<User> users = userDao.findAll();
		LOG.trace("Obtained users size --> " + users);
		
		request.setAttribute("users", users);
		
		LOG.trace("Forward address --> " + ViewPath.USERS_DETAILS);
		request.getRequestDispatcher(ViewPath.USERS_DETAILS).forward(request, response);
	}

	/**
	 * Find a user from DB.
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getOneUser(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("AuthService method: getOneUser starts");
		
		String userIdStr = request.getParameter("id");
		LOG.trace("Request parameter: id  --> " + userIdStr);
		
		Long userId = Helper.getLong(userIdStr);
		if (userId > 0) {
			User user = userDao.findById(userId);
			if (user != null) {
				request.setAttribute("u", user);
				LOG.trace("Request attribute 'u' (user) has been setted up --> " + user);
			} else {
				request.setAttribute("errorMsg", "Can't find user with id: " + userId);
				LOG.trace("Can't find user with id: " + userId);
			}
		} else {
			request.setAttribute("errorMsg", "wrong id: " + userIdStr);
			LOG.trace("id is not a number: " + userIdStr);
		}
		LOG.trace("Forward address --> " + ViewPath.USERS_DETAILS);
		request.getRequestDispatcher(ViewPath.USERS_DETAILS).forward(request, response);

	}

	/**
	 * Change user status 'blocked'.
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void blockUser(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOG.debug("AuthService method: blockUser starts");
		
		String action = request.getParameter("action");
		LOG.trace("Request parameter: action  --> " + action);
		
		boolean blocked = false;
		switch (action) {
		case "block":
			blocked = true;
			break;
		case "unblock":
			blocked = false;
			break;
		default:
			LOG.trace("wrong action parameter: action = " + action);
			break;
		}
		
		String id = request.getParameter("id");
		LOG.trace("Request parameter: id  --> " + id);
		
		Long userId = Helper.getLong(id);
		if (userId > 0) {
			User user = userDao.updateUserBlock(userId, blocked);
			request.setAttribute("u", user);
		} else {
			request.setAttribute("errorMsg", "Wrong user id.");
			LOG.debug("Wrong user id.");
		}
		LOG.trace("Forward address --> " + ViewPath.USERS_DETAILS);
		request.getRequestDispatcher(ViewPath.USERS_DETAILS).forward(request, response);
	}

}
