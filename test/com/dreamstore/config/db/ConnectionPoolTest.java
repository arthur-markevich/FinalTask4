package com.dreamstore.config.db;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import com.dreamstore.exception.PersistenceException;

class ConnectionPoolTest {

	Connection conn;
	Statement stm;

	@Before
	public void before() {
		conn = ConnectionPool.getConnection();
	}

	@After
	public void after() throws PersistenceException, SQLException {
		ConnectionPool.getConnection().close();
	}

	@Test
	void testGetConnection() {
		Connection conn = ConnectionPool.getConnection();
		assertNotNull(conn);
	}

}
