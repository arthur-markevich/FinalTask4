<head>

		<%-- <c:set var="previousPath" value="${requestScope['javax.servlet.forward.request_uri']}" scope="session"></c:set> --%>

	<title>${title}</title>

	
	
<%--=========================================================================== 
If you define http-equiv attribute, set the content type and the charset the same
as you set them in a page directive.
===========================================================================--%> 
<!-- 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
	
	<%--=========================================== T ========================================= --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/styles/bootstrap4/bootstrap.min.css">
<link href="${rootPath }/resources/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/styles/categories_styles.css">
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/styles/categories_responsive.css">
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/styles/responsive.css"> 

<link rel="stylesheet" href="${rootPath }/resources/plugins/themify-icons/themify-icons.css">
<%-- <link rel="stylesheet" type="text/css" href="${rootPath }/resources/plugins/jquery-ui-1.12.1.custom/jquery-ui.css"> --%>




<link rel="stylesheet" type="text/css" media="screen" href="${rootPath }/style/style.css"/> 


	<%--=========================================== T ========================================= --%>

</head>