<%--======================================================================================--%>

<header class="header trans_300">

<!-- Top Navigation -->

		<div class="top_nav">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="top_nav_left">
						
						<%-- message if user is blocked --%>
						<c:if test="${sessionScope.user.blocked eq true }">
						<span style="color: red"><fmt:message key="message.blocked"/></span>
						</c:if>
						<%--====== warn messages ======--%>
						<c:if test="${not empty errorMsg }">
							<span style="color:red">${errorMsg}</span>
							<% request.getSession().removeAttribute("errorMsg"); %>
						</c:if>
						<%--======= green messages =========--%>
						<c:if test="${not empty message }">
							<span style="color:green">${message}</span>
							<% request.getSession().removeAttribute("message"); %>
						</c:if>
						
						<%-- if user not logged in --%>
										<c:if test="${empty sessionScope.user}">
											<form action="${rootPath }/signin" method="post">
												<label><fmt:message key="label.username"/></label> 
												<input type="text" name="username"> 
												<label><fmt:message key="label.password"/></label>
												<input type="password" name="password"> 
												<input type="submit" value="<fmt:message key="link.signin"/>" class="btnx white">
												<input class="btnx white" type="button" value="<fmt:message key="link.signup"/>" onclick="window.location.href='${rootPath }/signup'" />
											</form>
								
										</c:if>
						
						</div>
					</div>
					<div class="col-md-5 text-right">
						<div class="top_nav_right">
							<ul class="top_nav_menu">
					<%--=============== Language ===============--%>
								<c:if test="${not empty applicationScope.languages }">
									<li class="language">
									<c:forEach var="l" items="${languages }">
												<c:if test="${l.id eq lid }">
													<a href="#">
														${l.name }
													<i class="fa fa-angle-down"></i>
													</a>
												</c:if>
										</c:forEach>
											
										
										<ul class="language_selection">
										<%--=================--%>
											
												<form>
										            <select id="language" name="language" onchange="submit()">
										            	<c:forEach items="${applicationScope.locales}" var="locale">
															<c:set var="selected" value="${locale.key == language ? 'selected' : '' }"/>
															<li><option value="${locale.key}" ${selected}>${locale.value}</option></li>
														</c:forEach>
										            
										            </select>
										        </form>
											
										<%--=================--%>
											
										</ul>
									</li>
								</c:if>	
							<%--=============== Language ===============--%>
							
							<%-- === If user role is admin === --%>
							<c:if test="${sessionScope.user.roleId == 1 }">
								<li class="currency">
									<a href="#">
										<fmt:message key="admin.admin"/>
										<i class="fa fa-angle-down"></i>
									</a>
									<ul class="currency_selection">
										<li><a href="${rootPath }/admin/orders"><fmt:message key="admin.orders"/></a></li> 
										<li><a href="${rootPath }/admin/users"><fmt:message key="admin.users"/></a></li>
										<li><a href="${rootPath }/admin/products/add"><fmt:message key="admin.add_product"/></a></li>
										<li><a href="${rootPath }/admin/categories/add"><fmt:message key="admin.add_category"/></a></li>
									</ul>
								</li>
								
							</c:if>
								
											
									<%-- if user logged in --%>
										<c:if test="${not empty sessionScope.user}">
								<li class="account">
										<a href="${rootPath }/user/profile"> ${sessionScope.user.firstname} ${sessionScope.user.lastname} <i class="fa fa-angle-down"></i></a>
											<ul class="account_selection">
														<li><a href="${rootPath }/signout"><i class="fa fa-user-plus" aria-hidden="true"></i><fmt:message key="link.signout"/></a></li>
													</ul>
								</li>				
										</c:if>
								
									
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Main Navigation -->

		<div class="main_nav_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-right">
						<div class="logo_container">
							<a href="${rootPath }">dream<span>store</span></a>
						</div>
						<!-- Navbar  -->
						<nav class="navbar">
							<ul class="navbar_menu">
							<li><a href="${rootPath }/products"><fmt:message key="text.all"/></a></li>
							<c:if test="${fn:length(categories) > 0 }">
								<c:forEach items="${categories[lid]}" var="category">
									<li><a href="${rootPath }/categories?id=${category.categoryId }">${category.name}</a></li>
								</c:forEach>
							</c:if>
							</ul>
						
							<ul class="navbar_user">
								
								<li class="checkout">
									<a href="${rootPath }/cart">
										<i class="fa fa-shopping-cart" aria-hidden="true"></i>
										
										<%-- if cart has stuff --%>
								<c:if test="${fn:length(sessionScope.cart) gt 0 }">
								<span id="checkout_items" class="checkout_items">
										${sessionScope.quantity }
								</span>
								</c:if>
									</a>
								</li>
							</ul>
							<div class="hamburger_container">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
<%--======================================================================================--%>

</header>