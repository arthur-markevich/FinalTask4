<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Administrator page" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
	<h1>Admin page</h1>
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>