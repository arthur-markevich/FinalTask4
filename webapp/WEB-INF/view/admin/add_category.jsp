<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Add new category" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
<div class="super_container">
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
		<div class="container">
		<div class="main_slider">
		<div class="container fill_height"> 
	<ul class="navbar_menu">	
	<c:forEach items="${categories[lid]}" var="category">
	<li><a href="${rootPath }/admin/categories?id=${category.categoryId }">${category.name}</a></li>
	</c:forEach>
	</ul>

	<form action="${rootPath }/admin/categories/add" method="post">
		<c:forEach items="${languages}" var="language">
			<label>${language.name}</label>
			<input type="text" name="${language.id}" >
			<br>
		</c:forEach>
		<input type="submit" class="btnx white"  value="add category">
	</form>
	</div>
	</div>
	</div>
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</div>
</body>
</html>