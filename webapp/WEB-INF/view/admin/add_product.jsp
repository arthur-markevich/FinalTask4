<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Add new product" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
<div class="main_slider">
		<div class="container align-items-center fill_height"> 
	<form action="${rootPath }/admin/products/add" method="post" enctype="multipart/form-data" >
		<table>
			<tr>
				<td>Product name:</td>
				<td>Category: <select name="category">
						<c:forEach items="${categories[lid]}" var="category">
							<option value="${category.categoryId}">${category.name}</option>
						</c:forEach>
				</select></td>
				<td><label>Price:</label> <input type="number" name="price"></td>
			</tr>
			<c:forEach items="${languages}" var="language">
				<tr>
				<td>
					<label>${language.name}</label>
					<input type="text" name="name[${language.id}]">
					</td>
					<td><textarea rows="8" cols="50" name="description[${language.id}]"></textarea></td>
				</tr>

			</c:forEach>
		</table>
		<%-- =========== File uploading ============== --%>
		 <table>
                <tr>
                    <td>Product image: </td>
                    <td><input type="file" name="img" size="50"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="add product">
                    </td>
                </tr>
            </table>
	</form>
	</div>
	</div>
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>
