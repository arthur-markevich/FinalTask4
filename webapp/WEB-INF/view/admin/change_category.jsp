<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Change category" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
	<div class="super_container">
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
		<div class="container">
		<div class="main_slider">
		<div class="container fill_height">
	<%-- ====== ct = List<CategoryTranslation> ====== --%>
	<c:if test="${not empty ct }">
		<c:if test="${not empty languageId }">
			<c:forEach var="t" items="${ct }">
				<c:if test="${t.languageId eq languageId }">
					<h4>${t.name }</h4>
				</c:if>	
			</c:forEach>
		</c:if>
		<form action="${rootPath }/admin/categories/change" method="post">
		<input type="hidden" name="id" value='<c:out value="${ct[0].categoryId }"></c:out>'>
		<c:forEach var="t" items="${ct }">
			<c:forEach var="l" items="${languages }">
				<c:if test="${t.languageId eq l.id }">
				<p>
					<label>${l.name }</label>
					<input type="text" name="${t.languageId }" value="${t.name }">
				</p>
				</c:if>
			</c:forEach>
		</c:forEach>
		<p><input type="submit" value="save"></p>
		</form>
	</c:if>
	</div>
	</div>
	</div>
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>
	</div>
</body>
</html>
