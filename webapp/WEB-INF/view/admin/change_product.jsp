<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Product Editor" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
	<div class="super_container">
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
		<div class="container fill_height">
		<div class="main_slider">

	<%-- =====   p = Product  pb = ProductBean  ===== --%>
	<c:if test="${not empty p and not empty pb}">

		<form action="${rootPath }/admin/products/change" enctype="multipart/form-data" method="post">
		<input type="hidden" name="id" value="${p.id }">
			<table>
				<tr>
					<td>Product name:</td>
					<td>Category: <select name="category">
							<c:forEach items="${categories[lid]}" var="category">
								<c:if test="${category.categoryId eq pb.categoryId}">
									<option value="${category.categoryId}" selected>${category.name}</option>
								</c:if>
								<option value="${category.categoryId}">${category.name}</option>
							</c:forEach>
					</select>
					</td>

					<td><label>Price:</label> <input type="number" name="price"
						value="${p.price }"></td>
				</tr>

				<%-- ----------------- /table head ---------------------- --%>

				<%-- ==================== translations = List<ProductTranslatoin> ==============  --%>
				<c:forEach items="${translations }" var="t">


					<c:forEach var="l" items="${languages }">
						<c:if test="${t.languageId eq l.id }">
							<tr>
								<td>
								<label><c:out value="${l.name }"></c:out>
								</label> 
								<input type="text" name="name[${l.id}]" value="${t.name }">
								</td>
								<td>
								<textarea rows="8" cols="50"
										name="description[${l.id}]">${t.description }</textarea>
								</td>
								</tr>
						</c:if>
						
					</c:forEach>

				</c:forEach>
					<%-- ============ Image ============== --%>
						<tr>
						<td colspan="2">
						<div class="crop">
							<img alt="${pb.name }" src="${rootPath }/${pb.imageURI }">
						</div>
						</td>
						</tr>
			</table>
			
				<%-- =========== File uploading ============== --%>
		<table style="border:0 !important">
			<tr>
				<td>Choose image:</td>
				<td><input type="file" name="img" size="50" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="save changes">
				</td>
			</tr>
		</table>

		</form>

		    <%-- ========================= c:if end ====================================  --%>
	</c:if>
	</div>
</div>
<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>
