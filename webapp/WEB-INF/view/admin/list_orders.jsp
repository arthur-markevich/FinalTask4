<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Orders" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
	<div class="main_slider">
		<div class="container fill_height"> 
		<div class="container height_10"> 
			<h3>All arders</h3>
		</div>
		<div class="container fill_height text-center "> 
		<div class="row fill_height">
		<div class="col-lg-12">
<%-- ===== users orders ===== --%>
<c:choose>
	<c:when test="${fn:length(ordersList) > 0}">
		<div class="divTable">
<div class="divTableBody">
<div class="divTableRow">
<div class="divTableHead">id</div>
<div class="divTableHead">user</div>
<div class="divTableHead">phone</div>
<div class="divTableHead">status</div>
<div class="divTableHead">date</div>
<div class="divTableHead">details</div>
</div>
			<c:forEach items="${ordersList }" var="o" >
				<div class="divTableRow">
					<div class="divTableCell">${o.id }</div>
					<div class="divTableCell"><a href="${rootPath }/admin/orders?id=${o.userId}&action=viewall">${o.firstname } ${o.lastname }</a></div>
					<div class="divTableCell">${o.phoneNumber }</div>
					<div class="divTableCell">${o.status }</div>
					<div class="divTableCell"><fmt:formatDate
						value="${o.createdAt }" pattern="yy MMM-dd HH:mm" /></div>
					<div class="divTableCell"><a href="${rootPath }/admin/orders?id=${o.id}&action=view">view</a></div>
				</div>
			</c:forEach>
		</div>
		</div>
	</c:when>
	<c:otherwise>
		<h3>No orders yet!</h3>
	</c:otherwise>
</c:choose>
</div>
</div>
</div>
</div>
</div>
<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>