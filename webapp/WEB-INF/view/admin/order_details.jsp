<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Order details" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>

	<%--===  Show order details from request  ====--%>
	<c:if test="${not empty orderBean }">
		<div class="main_slider" >
		<div class="container ">
			<div class="row align-items-center height_10">
				<div class="col text-center">
						<h2><a href="${rootPath }/admin/users?id=${orderBean.user.id }&action=view">${orderBean.user.firstname }
							${orderBean.user.lastname }</a></h2>
				</div>
			</div>
	<div class="row  fill_height_third">
	<div class="col-lg-3">
	<p>Status: ${orderBean.status } </p>
		<p>order id: ${orderBean.id }</p>
		<p><fmt:formatDate
						value="${orderBean.createdAt }" pattern="dd MMM-yy HH:mm" /></p>
	</div>
	<div class="col-lg-9">
		<table>
			
			<tr>
				<th>id</th>
				<th>quantity</th>
				<th>price</th>
				<th>Sub Total</th>
			</tr>
			<c:set value="0" var="s"></c:set>
			<c:forEach var="i" items="${orderBean.orderItems }">
				<c:set value="${s + i.pb.price * i.quantity}" var="s"></c:set>
				<tr>
					<td><a href="${rootPath }/products?id=${i.pb.id }">${i.pb.name }</a></td>
					<td>${i.quantity }</td>
					<td>${i.pb.price }</td>
					<td>${i.pb.price * i.quantity }</td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="3" align="right">Sum:</td>
				<td>${s }</td>
			</tr>
			<tr>
				<td colspan="2"><a
					href="${rootPath }/admin/orders?id=${orderBean.id }&action=paid"
					class="button">paid</a></td>
				<td colspan="2"><a
					href="${rootPath }/admin/orders?id=${orderBean.id }&action=cancel"
					class="button">cancel</a></td>
			</tr>
		</table>
	</div>
	
				</div> 
			</div>
		</div>
	</c:if>

<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>
