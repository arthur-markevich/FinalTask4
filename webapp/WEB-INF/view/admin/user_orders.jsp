<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="User orders" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
	
	
	<%-- ===== users orders List<OrderBean> ===== --%>
<c:choose>
	<c:when test="${fn:length(ordersList) > 0}">
	
	<div class="main_slider" >
		<div class="container fill_height_half orders_width">
			<div class="row align-items-center fill_height">
				<div class="col text-center">
					<div class="section_title">
						<h2><c:out value="${ordersList[0].user.firstname }"></c:out>
							<c:out value="${ordersList[0].user.lastname }"></c:out></h2>
					</div>
				</div>
			
					<div class="divTable">
						<div class="divTableBody">
						<div class="divTableRow">
							<div class="divTableHead">id</div>
							<div class="divTableHead">status</div>
							<div class="divTableHead">date</div>
							<div class="divTableHead">details</div>
							<div class="divTableHead">sum</div>
						</div>
						<c:forEach items="${ordersList }" var="o" >
							<div class="divTableRow">
								<div class="divTableCell">${o.id }</div>
								<div class="divTableCell">${o.status }</div>
								<div class="divTableCell"><fmt:formatDate
										value="${o.createdAt }" pattern="yy MMM-dd HH:mm" /></div>
								<div class="divTableCell"><a href="${rootPath }/admin/orders?id=${o.id}&action=view">view</a></div>
								<div class="divTableCell">${o.sum }</div>
							</div>
							
						</c:forEach>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<h3>No orders found!</h3>
	</c:otherwise>
</c:choose>

<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>	