<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Users details" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>

	<%-- === List of users === --%>
	<c:if test="${not empty users }">
		<c:choose>
			<c:when test="${fn:length(users) gt 0 }">
			
	

	<div class="main_slider">
	<div class="container fill_height">
			<div class="row align-items-center fill_height">
		
			<div class="row align-items-center height_10">
				<div class="col text-center">
						<h2>${title }</h2>
				</div>
				</div>
				<div class="container fill_height">
					<div class="divTable">
						<div class="divTableBody">
							<div class="divTableRow">
								<div class="divTableHead">id</div>
									<div class="divTableHead">Name</div>
									<div class="divTableHead">Phone</div>
									<div class="divTableHead">Email</div>
									<div class="divTableHead">Registered</div>
									<div class="divTableHead">Blocked</div>
									<div class="divTableHead">Total orders sum</div>
								</div>
							
					<c:forEach var="u" items="${users }">
						<div class="divTableRow">
							<div class="divTableCell">${u.id }</div>
							<div class="divTableCell"><a href="${rootPath }/admin/users?id=${u.id }&action=view">${u.firstname } ${u.lastname }</a></div>
							<div class="divTableCell">${u.phoneNumber }</div>
							<div class="divTableCell">${u.email }</div>
							<div class="divTableCell"><fmt:formatDate value="${u.createdAt }" pattern="dd MMM-yy HH:mm" /></div>
							<div class="divTableCell">
								<c:if test="${u.blocked eq true }"><span class="warn">Blocked</span></c:if> 
								<c:if test="${u.blocked eq false }">no</c:if>
							</div>
							<div class="divTableCell">${u.wasted }</div>
						</div>
					</c:forEach>
				</div>
				</div> 
				
				</div>
				</div>
				</div>
				</div>
				
			</c:when>
			<c:otherwise>
			No users in the system.
		</c:otherwise>
		</c:choose>
	</c:if>

	<%-- === One user details (u == user) === --%>
	<c:if test="${not empty u }">
	<div class="main_slider" >

			

		<div class="container fill_height_half">



			<div class="row align-items-center fill_height">

			<div class="col text-center">
					<div class="section_title">
						<h2>${u.firstname } ${u.lastname } <c:if test="${u.blocked eq true }"><span class="warn">Blocked</span></c:if></h2>
					</div>
				</div>
	<div class="divTable">
					<div class="divTableBody">
						<div class="divTableRow">
		<div class="divTableCell"><a href="${rootPath }/admin/orders?id=${u.id}&action=viewall">view orders</a></div></div>
		<div class="divTableRow"><div class="divTableCell">${u.username }</div></div>
		<div class="divTableRow"><div class="divTableCell">${u.phoneNumber }</div></div>
		<div class="divTableRow"><div class="divTableCell"><a href="mailto:${u.email }" target="_top">${u.email }</a></div></div>
		<div class="divTableRow"><div class="divTableCell">
		<c:if test="${u.blocked eq true }"><a
					href="${rootPath }/admin/users?id=${u.id }&action=unblock"
					class="button">Unblock</a></c:if>
		<c:if test="${u.blocked eq false }"><a
					href="${rootPath }/admin/users?id=${u.id }&action=block"
					class="button">Block</a></c:if>
		</div>
		</div>
		</div>
		</div>
		</div>
		</div>
		</div>
		
		
	</c:if>
	
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>
