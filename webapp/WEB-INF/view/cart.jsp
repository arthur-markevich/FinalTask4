<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" scope="page"><fmt:message key="page.cart"/></c:set>
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
	<div class="main_slider">
		<div class="container fill_height"> 
	<c:choose>
	<c:when test="${fn:length(sessionScope.cart) == 0 }">
		<div class="row align-items-center fill_height">
				<div class="col text-center">
						<h3><fmt:message key="text.empty_cart"/><a href="${rootPath }/products">-></a></h3>
				</div></div>
	</c:when>
	<c:otherwise>
	
	
			<div class="row height_10">
				<div class="col text-left">
					<div class="section_title">
						<h3><fmt:message key="page.cart"/></h3>
					</div>
				</div>
			</div>
				<div class="row fill_height_third text-center ">
				<div class="col-lg-8">
				<div class="divTable">
					<div class="divTableBody">
						<div class="divTableRow">
							<div class="divTableHead"><fmt:message key="text.option"/></div>
							<div class="divTableHead"><fmt:message key="text.name"/></div>
							<div class="divTableHead"><fmt:message key="text.price"/></div>
							<div class="divTableHead"><fmt:message key="text.quantity"/></div>
							<div class="divTableHead"><fmt:message key="text.sub_total"/></div>
							</div>
						<c:set value="0" var="s"></c:set>
						<c:forEach var="it" items="${sessionScope.cart }">
							<c:set value="${s + it.pb.price * it.quantity}" var="s"></c:set>
							<div class="divTableRow">
								<div class="divTableCell"><a href="${rootPath }/cart?id=${it.pb.id }&action=remove"
														onclick="return confirm('<fmt:message key="text.are_you_sure"/>')"><fmt:message key="text.delete"/></a></div>
								<div class="divTableCell"><a href="${rootPath }/products?id=${it.pb.id }">${it.pb.name }</a></div>
								<div class="divTableCell">${it.pb.price }</div>
								<div class="divTableCell">${it.quantity }</div>
								<div class="divTableCell">${it.pb.price * it.quantity}</div>
							</div>
						</c:forEach>
						</div>
					</div>
					 <div class="m-t-lg">
				        <ul class="list-inline">
				          <li>
				            <span style="color:red;"><fmt:message key="text.sum"/>: </span>${s}
				          </li>
				        </ul>
				      </div>
				</div>
				<div class="col-lg-4">
				<p><a href="${rootPath }/cart?&action=buy"><fmt:message key="text.buy_now"/></a></p>
				<p> <a href="${rootPath }/products"><fmt:message key="text.continue_shopping"/></a></p>
				</div>
			</div>
	
	
	</c:otherwise>
	</c:choose>
	</div>
	
</div>
	
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>