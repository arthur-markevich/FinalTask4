<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Categories" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
<div class="super_container">
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
		<div class="container">
		<div class="main_slider">
		<div class="container fill_height"> 
	<ul class="navbar_menu">	
	<c:forEach items="${categories[lid]}" var="category">
	<li><a href="${rootPath }/admin/categories?id=${category.categoryId }">${category.name}</a></li>
	</c:forEach>
	</ul>

	
	</div>
	</div>
	</div>
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>

</body>
</html>