<%@ page isErrorPage="true" %>  
<%@ page import="java.io.PrintWriter" %>
<%@ include file="/WEB-INF/include/taglibs.jspf"%>

<html>
<c:set var="title" value="Error" scope="page" />
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
<div class="super_container">

	<%@ include file="/WEB-INF/fragments/header.jsp"%>

	<div class="fs_menu_overlay"></div>

	

	<div class="container product_section_container">
		<div class="row">
			<div class="col product_section clearfix">

				<!-- Breadcrumbs -->

				<div class="breadcrumbs d-flex flex-row align-items-center">
					<%-- <ul>
						<li><a href="index.html">Home</a></li>
						<li class="active"><a href="index.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Men's</a></li>
					</ul> --%>
				</div>

				<%-- Sidebar --%>

				<div class="sidebar"></div>

				<!-- Main Content -->

				<div class="main_content fill_height">

			<%-- CONTENT --%>
				
				<h2 class="error">
					The following error occurred
				</h2>
			
				<%-- this way we obtain an information about an exception (if it has been occurred) --%>
				<c:set var="code" value="${requestScope['javax.servlet.error.status_code']}"/>
				<c:set var="message" value="${requestScope['javax.servlet.error.message']}"/>
				<c:set var="exception" value="${requestScope['javax.servlet.error.exception']}"/>
				
				<c:if test="${not empty code}">
					<h3>Error code: ${code}</h3>
				</c:if>			
				
				<c:if test="${not empty message}">
					<h3>${message}</h3>
				</c:if>
				
				<c:if test="${not empty exception}">
					<% exception.printStackTrace(new PrintWriter(out)); %>
				</c:if>
				
				<%-- if we get this page using forward --%>
				<c:if test="${not empty requestScope.errorMessage}">
					<h3>${requestScope.errorMessage}</h3>
				</c:if>

			<%-- CONTENT --%>
					
			</div>
		</div>
	</div>

	
</div>
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>


</body>

</html>
