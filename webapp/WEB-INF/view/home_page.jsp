<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" scope="page"><fmt:message key="page.home"/></c:set>
<html>
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
<div class="super_container">
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
		<!-- Slider -->

	<div class="main_slider" style="background-image:url(images/slider_1.jpg)">
		<div class="container fill_height">
			<div class="row align-items-center fill_height">
				<div class="col">
					<div class="main_slider_content">
						<h6><fmt:message key="home.text.main.little"/></h6>
						<h1><fmt:message key="home.text.main.big"/></h1>
						<div class="red_button shop_now_button"><a href="${rootPath }/products"><fmt:message key="home.text.main.shop_now"/></a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	
		
	
<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>
