<%@ include file="/WEB-INF/include/taglibs.jspf"%>

<html>
<c:set var="title" value="Restrict" scope="page" />
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
<div class="super_container">

	<!-- Header -->

	<%@ include file="/WEB-INF/fragments/header.jsp"%>

	<div class="fs_menu_overlay"></div>

	

	<div class="container product_section_container">
		<div class="row">
			<div class="col product_section clearfix">

				<!-- Breadcrumbs -->

				<div class="breadcrumbs d-flex flex-row align-items-center">
					<%-- <ul>
						<li><a href="index.html">Home</a></li>
						<li class="active"><a href="index.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Men's</a></li>
					</ul> --%>
				</div>

				<%-- Sidebar --%>

				<div class="sidebar"></div>

				<!-- Main Content -->

				<div class="main_content fill_height">

			<%-- CONTENT --%>
				
				<h3>To place an order you have to sign in! <a href="${rootPath }/signup">Sign Up</a></h3>

			<%-- CONTENT --%>
					
			</div>
		</div>
	</div>

	
</div>
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>


</body>

</html>
