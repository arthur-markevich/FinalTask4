<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:if test="${not empty pb }"><c:set var="title" value="${pb.name }" scope="page" /></c:if>
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/styles/single_styles.css">
<link rel="stylesheet" type="text/css" href="${rootPath }/resources/styles/single_responsive.css">
<html>
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
<div class="super_container">
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
	<%-- =====   pb = ProductBean    ===== --%>
	<c:if test="${not empty pb }">
	
		<div class="container single_product_container">
		<div class="row">
			<div class="col">

				<!-- Breadcrumbs -->

				<div class="breadcrumbs d-flex flex-row align-items-center">
					

				</div>

			</div>
		</div>

		<div class="row">
			<div class="col-lg-7">
				<div class="single_product_pics">
					<div class="row">
						<div class="col-lg-3 thumbnails_col order-lg-1 order-2">
							

						</div>
						<div class="col-lg-9 image_col order-lg-2 order-1">
							<div class="single_product_image">
								<div class="single_product_image_background" style="background-image:url(${rootPath }/${pb.imageURI })"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="product_details">
					<div class="product_details_title">
						<h2>${pb.name }t</h2>
						<p>${pb.description }</p>
					</div>
					<div class="free_delivery d-flex flex-row align-items-center justify-content-center">
						<span class="ti-truck"></span><span>free delivery</span>
					</div>
					<div class="original_price">$${pb.price * 2 }</div>
					<div class="product_price">$${pb.price }</div>
					

					<div class="quantity d-flex flex-column flex-sm-row align-items-sm-center">
						<div class="red_button" style="width:150; background:#fe4c50"><a href="${rootPath }/cart?id=${pb.id }&action=add"><fmt:message key="link.order_now"/></a></div>
					</div>
					<c:if test="${not empty sessionScope.user }">
						<c:if test="${sessionScope.user.roleId == 1 }">
							<div class="quantity d-flex flex-column flex-sm-row align-items-sm-center">
						<div class="red_button" style="width:150; background:#fe4c50"><a href="${rootPath }/admin/products?id=${pb.id }&action=change"><fmt:message key="link.change"/></a></div>
					</div>
					<div class="quantity d-flex flex-column flex-sm-row align-items-sm-center">
						<div class="red_button" style="width:150; background:#fe4c50"><a href="${rootPath }/admin/products?id=${pb.id }&action=delete"><fmt:message key="link.delete"/></a></div>
					</div>
						</c:if>
						</c:if>
				</div>
			</div>
		</div>

	</div>
		
		
	
		
</c:if>
<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>
