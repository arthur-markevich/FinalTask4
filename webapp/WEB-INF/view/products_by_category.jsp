<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Product by category" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
<div class="super_container">

	<!-- Header -->

	<%@ include file="/WEB-INF/fragments/header.jsp"%>

	<div class="fs_menu_overlay"></div>

	

	<div class="container product_section_container">
		<div class="row">
			<div class="col product_section clearfix">

				<!-- Breadcrumbs -->

				<div class="breadcrumbs d-flex flex-row align-items-center">
					<%-- --- --%>
				</div>

				<!-- Sidebar -->

				<div class="sidebar">
					

					<!-- Price Range Filtering -->
					<div class="sidebar_section">
						<div class="sidebar_title">
							<h5><fmt:message key="text.filter_by_price"/></h5>
						</div>
						<p>
							<input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
						</p>
						<div id="slider-range"></div>
						<div class="filter_button"><span><fmt:message key="text.filter"/></span></div>
					</div>



				</div>

				<!-- Main Content -->

				<div class="main_content">

					<!-- Products -->

					<div class="products_iso">
						<div class="row">
							<div class="col">

								<!-- Product Sorting -->

								<div class="product_sorting_container product_sorting_container_top">
									<ul class="product_sorting">
										<li>
											<span class="type_sorting_text"><fmt:message key="text.sort.default_sort"/></span>
											<i class="fa fa-angle-down"></i>
											<ul class="sorting_type">
												<li class="type_sorting_btn" data-isotope-option='{ "sortBy": "original-order" }'><span><fmt:message key="text.sort.default_sort"/></span></li>
												<li class="type_sorting_btn" data-isotope-option='{ "sortBy": "price" }'><span><fmt:message key="text.sort.price"/></span></li>
												<li class="type_sorting_btn" data-isotope-option='{ "sortBy": "name" }'><span><fmt:message key="text.sort.product_name"/></span></li>
												<li class="type_sorting_btn" data-isotope-option='{ "sortBy": "date" }'><span><fmt:message key="text.sort.new"/></span></li>
											</ul>
										</li>
										<li>
											<span><fmt:message key="text.show"/></span>
											<span class="num_sorting_text">6</span>
											<i class="fa fa-angle-down"></i>
											<ul class="sorting_num">
												<li class="num_sorting_btn"><span>6</span></li>
												<li class="num_sorting_btn"><span>12</span></li>
												<li class="num_sorting_btn"><span>24</span></li>
											</ul>
										</li>
									</ul>
									


								</div>

								<!-- Product Grid -->

								<div class="product-grid">

								<%-- === productsToPage - List<ProductBean> --%>
									<c:forEach var="pb" items="${productsToPage }">
									<!-- Products -->
									<div class="product-item men">
										<div class="product discount product_filter">
											<div class="product_image">
												<img src="${pb.imageURI }" alt="${pb.name }">
											</div>
											<div class="product_info">
												<h6 class="product_name"><a href="${rootPath }/products?id=${pb.id }">${pb.name }</a></h6>
												<div class="product_price">$ ${pb.price }</div>
												<div  style="display:none" class="product_date"><fmt:formatDate
						value="${pb.createdAt }" pattern="yyyy-MMM-dd HH:mm:ss" /></div>
											</div>
										</div>
										<div class="red_button add_to_cart_button"><a href="${rootPath }/cart?id=${pb.id }&action=add"><fmt:message key="link.add_to_cart"/></a></div>
									</div>
									</c:forEach>	

							

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
</div>
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>


</body>

</html>
