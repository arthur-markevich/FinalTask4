<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Sign up" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
 <div class="main_slider"> 
	<div class="container align-items-center fill_height "> 
		<div class="row align-items-center height_10">
			<div class="col text-center">
					<div class="section_title">
						<h3><fmt:message key="page.signup"/></h3>
					</div>
				</div>
			</div> 
<div class="container align-items-center fill_height orders_width "> 
<!-- <div class="row align-items-center fill_height ">  -->
	<div class="container__child signup__form">
    <form action="signup" method="post">
      <div class="form-group">
        <label for="username"><fmt:message key="label.username"/></label>
        <input class="form-control" type="text" name="username" id="username" placeholder="<fmt:message key="placeholder.username"/>" required />
      </div>
      <div class="form-group">
        <label for="email"><fmt:message key="label.email"/></label>
        <input class="form-control" type="text" name="email" id="email" placeholder="james.bond@spectre.com" required />
      </div>
      <div class="form-group">
        <label for="username"><fmt:message key="label.firstname"/></label>
        <input class="form-control" type="text" name="firstname" id="username" placeholder="<fmt:message key="placeholder.firstname"/>" required />
      </div>
      <div class="form-group">
        <label for="username"><fmt:message key="label.lastname"/></label>
        <input class="form-control" type="text" name="lastname" id="username" placeholder="<fmt:message key="placeholder.lastname"/>" required />
      </div>
      <div class="form-group">
        <label for="username"><fmt:message key="label.contact_number"/></label>
        <input class="form-control" type="text" name="phonenumber" id="username" pattern="\d{11}" placeholder="380**********" required />
      </div>
       
      <div class="form-group">
        <label for="password"><fmt:message key="label.password"/></label>
        <input class="form-control" type="password" name="password" id="password" placeholder="********" required />
      </div>
      <div class="form-group">
        <label for="passwordRepeat"><fmt:message key="label.password.repeat"/></label>
        <input class="form-control" type="password" name="passwordRepeat" id="passwordRepeat" placeholder="********" required />
      </div>
      <div class="m-t-lg">
        <ul class="list-inline">
          <li>
            <input class="btn btn--form" type="submit" value="<fmt:message key="link.signup"/>" />
          </li>
        </ul>
      </div>
    </form>  
  </div>
</div>
</div>
<!-- </div> -->
<!-- <form action="signup" method="post">
	<label>First name</label>
	<input type="text" name="firstname"/><br>
	<label>Last name</label>
	<input type="text" name="lastname"/><br>
	<label>Login</label>
	<input type="text" name="username"/><br>
	<label>email</label>
	<input type="email" name="email"/><br>
	<label>Phone number</label>
	<input type="text" name="phonenumber" /><br>
	<label>Password</label>
	<input type="password" name="password" /><br>
	<label>Repeat Password</label>
	<input type="password" name="password2" /><br>
	<label>Date of Birth</label>
	<input type="date" name="date"><br>
	<input type="submit" value="sign up">
</form> -->
</div> 
<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>