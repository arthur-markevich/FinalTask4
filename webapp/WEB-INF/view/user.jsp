<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="User --- Page" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
	<h1>User page</h1>
	
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>