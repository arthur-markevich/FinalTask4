<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Order details" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp"%>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
	<%--===  Show order details from request (OrderBean order)  ====--%>
	<div class="main_slider">
		<div class="container fill_height"> 
	<c:if test="${not empty order }">
		<table>
			<tr>
				<th colspan="4">[${order.id }] ${order.status } <fmt:formatDate
						value="${order.createdAt }" pattern="dd-MMM-yy HH:mm" /></th>
			</tr>
			<tr>
				<th>id</th>
				<th>quantity</th>
				<th>price</th>
				<th>Sub Total</th>
			</tr>
			<c:set value="0" var="s"></c:set>
			<c:forEach var="i" items="${order.orderItems }">
				<c:set value="${s + i.pb.price * i.quantity}" var="s"></c:set>
				<tr>
					<td><a href="${rootPath }/products?id=${i.pb.id }">${i.pb.name }</a></td>
					<td>${i.quantity }</td>
					<td>${i.pb.price }</td>
					<td>${i.pb.price * i.quantity }</td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="3" align="right">Sum:</td>
				<td>${s }</td>
			</tr>
		</table>
	</c:if>
	<c:if test="${empty order }">
		no order
	</c:if>
	</div>
	</div>
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>
