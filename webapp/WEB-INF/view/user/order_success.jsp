<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Order success" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
	
	<%-- ============================================ 
		It's better o change session scope to request ! ! !
		 ============================================= --%>
	
	<h1>Order success</h1>
	
		<c:if test="${not empty sessionScope.orderBean }">
			Your order ${sessionScope.orderBean.id } <br>
			Created at ${sessionScope.orderBean.createdAt } <br>
			Status ${sessionScope.orderBean.status } <br>
		</c:if>
		<c:if test="${empty sessionScope.orderBean }">
			You have no orders!
		</c:if>
		
		<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>