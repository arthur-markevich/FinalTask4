<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="Orders" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
	<div class="main_slider">
		<div class="container fill_height orders_width"> 
				<c:choose>
				<%-- List<OrderBean> orders --%>
				<c:when test="${fn:length(orders) gt 0 }">
				<div class="row align-items-center height_10">
				<div class="col text-center">
						<h2>Your orders:</h2>
				</div></div>
				
				<div class="divTable">
					<div class="divTableBody">
						<div class="divTableRow">
							<div class="divTableHead">id</div>
							<div class="divTableHead">status</div>
							<div class="divTableHead">date</div>
							<div class="divTableHead">details</div>
						</div>
						<c:forEach items="${orders }" var="o" >
						<div class="divTableRow">
							<div class="divTableCell">${o.id }</div>
							<div class="divTableCell">${o.status }</div>
							<div class="divTableCell"><fmt:formatDate value="${o.createdAt }" pattern="dd MMM-yy HH:mm" /></div>
							<div class="divTableCell"><a href="${rootPath }/user/orders?id=${o.id}&action=view">view</a></div>						
						</div>
					</c:forEach>
				</div>
				</div>
				</c:when>
				<c:otherwise>
				<div class="row align-items-center fill_height">
				<div class="col text-center">
						<h3><fmt:message key="home.text.main.shop_now"/> <a href="${rootPath }/products">-></a></h3>
				</div></div>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>	