<%@ include file="/WEB-INF/include/taglibs.jspf"%>
<c:set var="title" value="${sessionScope.user.username } profile" scope="page" />
<html>
<%@ include file="/WEB-INF/fragments/head.jsp" %>
<body>
	<%@ include file="/WEB-INF/fragments/header.jsp"%>
	<div class="super_container  ">
		
			<div class="row fill_heigh_10 text-left orders_width">
			<p><a href="${rootPath }/user/orders" ><fmt:message key="text.orders"/></a></p>
			</div>
<div class="container orders_width"> 
<c:set var="u" value="${sessionScope.user }" ></c:set>
<h3><fmt:message key="text.created_at"/> <fmt:formatDate value="${u.createdAt }" pattern="dd-MMM yyyy" /></h3>
<%-- === User data change === --%>
<div class="row  fill_heigh " >
<div class="container__child signup__form">
<form action="${rootPath }/user" method="post">

 <div class="form-group">
	<label><fmt:message key="label.firstname"/></label>
	<input class="form-control" type="text" name="firstname" value="${u.firstname }">
	</div>
	
	 <div class="form-group">
	<label for="username"><fmt:message key="label.lastname"/></label>
	<input class="form-control" type="text" name="lastname" value="${u.lastname }"><br>
	</div>
	
	 <div class="form-group">
	<label for="username"><fmt:message key="label.contact_number"/></label>
	<input class="form-control" type="text" name="phonenumber" value="${u.phoneNumber }"><br>
	</div>
	
	 <div class="form-group">
	<label for="email"><fmt:message key="label.email"/></label>
	<input class="form-control" type="text" name="email" value="${u.email }"><br>
	</div>
	
	 <div class="form-group">
	<label for="username"><fmt:message key="label.username"/></label>
	<input class="form-control" type="text" name="username" value="${u.username }"><br>
	</div>
	
	 <div class="form-group">
	<label for="username"><fmt:message key="label.password"/></label>
	<input class="form-control" type="password" name="password"><br>
	</div>
	<div class="m-t-lg">
        <ul class="list-inline">
          <li>
            <input class="btn btn--form" type="submit" value=<fmt:message key="button.save"/>>
          </li>
        </ul>
      </div>
	
</form>	
</div>
</div>
	</div>
	
	<%@ include file="/WEB-INF/fragments/footer.jsp"%>
</body>
</html>